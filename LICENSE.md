Copyright 2017 Iceberg Software, LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


This software requires [Apache XmlSchema](http://ws.apache.org/xmlschema/)
which is licensed under the Apache License 2.0. Note that the xmlschema-core
is copied into this project and modified; thus a custom version of xmlschema-core
is used within this project.

The icons used in this software are currently copied from
[Eclipse](https://www.eclipse.org/) and are licensed under the
[Eclipse Software Foundation User Agreement](https://www.eclipse.org/legal/epl/notice.php).
