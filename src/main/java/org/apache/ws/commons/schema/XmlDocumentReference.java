package org.apache.ws.commons.schema;

import org.w3c.dom.Element;

/**
 * This is an interface that tells us to preserve a reference to the DOM object
 * in the source XSD schema.
 */
public interface XmlDocumentReference
{
	public void setElementReference(Element el);
	public Element getElementReference();
}
