package com.iceberg.chill;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.iceberg.chill.model.ChillTreeMenuProvider;
import com.iceberg.chill.model.ChillTreeModel;
import com.iceberg.chill.model.xsd.XsdTreeModel;
import com.iceberg.chill.model.xsd.util.EnumValueFilter;
import com.iceberg.chill.model.xsd.util.TypeNameFilter;

/**
 * This is the main pane for a tree. It's a scroll pane with an embedded JTree.
 * If you want to embed the viewer in an existing application, you'd use this
 * component and embed it.
 * 
 * Call the openFile() method to load content into the JTree.
 */
public class ChillTreePane extends JScrollPane
{
	private static final long serialVersionUID = 1L;
	
	private JTree tree;
	private TreeModel model;
	
	public ChillTreePane()
	{
		initializeTree();
	}

	private void initializeTree()
	{
	    tree = new JTree();
	    tree.setModel(new DefaultTreeModel(null));
	    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    tree.setCellRenderer(new ChillTreeCellRenderer());
	    this.setPreferredSize(new Dimension(400, 600));
	    
	    tree.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mousePressed(MouseEvent e)
	    	{
	    		if (e.isPopupTrigger())
	    		{
	    			popupContextMenu(e);
	    		}
	    	}
	    	
	    	@Override
	    	public void mouseReleased(MouseEvent e)
	    	{
	    		if (e.isPopupTrigger())
	    		{
	    			popupContextMenu(e);
	    		}
	    	}
	    });
	    
	    this.setViewportView(tree);
	}

	public JTree getTree()
	{
		return tree;
	}

	public void openFile(File file)
	{
		try
		{
			model = FileLoaderFactory.loadFile(file);
			
			setModel(model);
		}
		catch (Exception ex)
		{
			java.util.logging.Logger.getLogger("chill").log(Level.SEVERE, ex.getMessage(), ex);

			JOptionPane.showMessageDialog(this,
				    "Failed to load file: " + ex.getMessage(),
				    "File open failed",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void setModel(TreeModel model)
	{
		this.model = model;
		
		tree.setModel(model);
		
		activateChillTreeModel();
	}

	/**
	 * If our model is a ChillTreeModel, we tell it about tree.
	 * This is needed to setup the ChillDocumentNode knowing about
	 * trees and models (which some operations will need).
	 */
	private void activateChillTreeModel()
	{
		if (model instanceof ChillTreeModel)
		{
			ChillTreeModel cModel = (ChillTreeModel) model;
			cModel.setTree(tree);
		}
	}

	protected void popupContextMenu(MouseEvent e)
	{
        int row = tree.getClosestRowForLocation(e.getX(), e.getY());
        tree.setSelectionRow(row);
        
		JPopupMenu menu = createPopupMenu((TreeNode)tree.getPathForRow(row).getLastPathComponent());
		menu.show(e.getComponent(), e.getX(), e.getY());
	}

	private JPopupMenu createPopupMenu(TreeNode node)
	{
	    JPopupMenu popup = new JPopupMenu();
	    
	    JMenuItem menuItem = new JMenuItem("Expand All");
	    menuItem.addActionListener((l) -> expandAll());
	    popup.add(menuItem);
	    
	    menuItem = new JMenuItem("Hide All");
	    menuItem.addActionListener((l) -> hideAll());
	    popup.add(menuItem);
	    
	    if (model instanceof XsdTreeModel)
	    {
		    menuItem = new JMenuItem("Type Hierarchy");
		    menuItem.addActionListener((l) -> typeHierarchy());
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("Find Enum...");
		    menuItem.addActionListener((l) -> findEnumMenuAction());
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("Find Type...");
		    menuItem.addActionListener((l) -> findTypeMenuAction());
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("As text...");
		    menuItem.addActionListener((l) -> displayAsTextAction());
		    popup.add(menuItem);
		    
		    menuItem = new JMenuItem("To csv...");
		    menuItem.addActionListener((l) -> generateCSVAction());
		    popup.add(menuItem);
	    }
	    
	    popup.addSeparator();
	    
	    // then add any options specific to this node
	    if (node instanceof ChillTreeMenuProvider)
	    {
	    	addCustomMenuOptions(popup, (ChillTreeMenuProvider)node);
	    }
	    
	    return popup;
	}

	private void addCustomMenuOptions(JPopupMenu popup, ChillTreeMenuProvider node)
	{
		Map<String, ActionListener> customMenuItems = node.getMenuOptions();
		for (String key : customMenuItems.keySet())
		{
			JMenuItem menuItem = new JMenuItem(key);
			menuItem.addActionListener(customMenuItems.get(key));
			popup.add(menuItem);
		}
	}

	private void expandAll()
	{
		int selectedRow = tree.getSelectionRows()[0];
		TreePath path = tree.getPathForRow(selectedRow);
		
		recursiveExpandPath(path);
	}

	private void recursiveExpandPath(TreePath path)
	{
		tree.expandPath(path);
		
		TreeNode node = (TreeNode)path.getLastPathComponent();
		int childCount = node.getChildCount();
		for (int i = 0; i < childCount; i++)
		{
			TreePath childPath = path.pathByAddingChild(node.getChildAt(i));
			recursiveExpandPath(childPath);
		}
	}

	private void hideAll()
	{
		int selectedRow = tree.getSelectionRows()[0];
		TreePath path = tree.getPathForRow(selectedRow);
		
		recursiveCollapsePath(path);
	}

	private void recursiveCollapsePath(TreePath path)
	{
		TreeNode node = (TreeNode)path.getLastPathComponent();
		int childCount = node.getChildCount();
		for (int i = 0; i < childCount; i++)
		{
			TreePath childPath = path.pathByAddingChild(node.getChildAt(i));
			recursiveExpandPath(childPath);
		}
		
		tree.collapsePath(path);
	}

	/**
	 * For XsdTreeModel only, we can show a type hierarchy for the tree types.
	 */
	private void typeHierarchy()
	{
		int selectedRow = tree.getSelectionRows()[0];
		TreePath path = tree.getPathForRow(selectedRow);
		TreeNode node = (TreeNode)path.getLastPathComponent();
		
		XsdTreeModel xsdModel = (XsdTreeModel)model;
		Container displayObj = xsdModel.getTypeHierarchyTreeForNode(node);
		
		displayTreeDialog(displayObj, "Type Tree for: " + node.toString());
	}

	/**
	 * Pops up a dialog containing this type tree that the Xsd model
	 * put together for us.
	 * 
	 * @param tree
	 */
	private void displayTreeDialog(Container displayObj, String title)
	{
		JOptionPane pane = new JOptionPane(JOptionPane.PLAIN_MESSAGE);
		JDialog dialog = pane.createDialog(null, title);
		dialog.setModal(false);
		dialog.setContentPane(displayObj);
		dialog.setResizable(true);
		dialog.setVisible(true);
	}

	private void findEnumMenuAction()
	{
		String enumValue = (String)JOptionPane.showInputDialog(
				"Enter enum value",	"");
		
		if (enumValue != null)
		{
			XsdTreeModel xsdModel = (XsdTreeModel)model;
			Container displayObj = xsdModel.createFilteredDialogObject(xsdModel.getSchema(), new EnumValueFilter(enumValue));
		
			displayTreeDialog(displayObj, "Enums with value: " + enumValue);
		}
	}

	private void findTypeMenuAction()
	{
		String filterString = (String)JOptionPane.showInputDialog(
				"Enter type name", "");
		
		if (filterString != null)
		{
			XsdTreeModel xsdModel = (XsdTreeModel)model;
			Container displayObj = xsdModel.createFilteredDialogObject(xsdModel.getSchema(), new TypeNameFilter(filterString));
		
			displayTreeDialog(displayObj, "Elements with type: " + filterString);
		}
	}

	private void displayAsTextAction()
	{
		int selectedRow = tree.getSelectionRows()[0];
		TreePath path = tree.getPathForRow(selectedRow);
		TreeNode node = (TreeNode)path.getLastPathComponent();
		
		StringBuilder sb = new StringBuilder();
		
		recursiveBuildTreeNodeString(sb, node, 0);
		
		displayTextDialog(sb.toString(), "Text of elements for: " + node.toString());
	}

	/**
	 * Adds the current node, then its children, recursively.
	 * We append the toString() for each node to the string builder.
	 * The indent is the amount of indent for this particular node.
	 * The value goes up or down by 1, but we could treat each number
	 * as multiple spaces, if desired.
	 * 
	 * @param sb
	 * @param node
	 * @param indent
	 */
	private void recursiveBuildTreeNodeString(StringBuilder sb, TreeNode node, int indent)
	{
		addIndent(sb, indent);
		sb.append(node.toString());
		sb.append("\n");
		addChildren(sb, node, indent);
	}

	private void addIndent(StringBuilder sb, int indent)
	{
		for (int i = 0; i < indent; i++)
		{
			sb.append("  ");
		}
	}

	private void addChildren(StringBuilder sb, TreeNode node, int indent)
	{
		for (int i = 0; i < node.getChildCount(); i++)
		{
			TreeNode child = node.getChildAt(i);
			recursiveBuildTreeNodeString(sb, child, indent+1);
		}
	}
	
	private void displayTextDialog(String text, String title)
	{
		JOptionPane pane = new JOptionPane(JOptionPane.PLAIN_MESSAGE);
		JDialog dialog = pane.createDialog(null, title);
		dialog.setModal(false);
		
		JTextArea textArea = new JTextArea();
		textArea.setText(text);
		textArea.setCaretPosition(0);
		textArea.setEditable(false);
		addCopyMenuToTextarea(textArea);
		
		JScrollPane scroll = new JScrollPane(textArea);
		dialog.setContentPane(scroll);
		dialog.setResizable(true);
		dialog.setPreferredSize(new Dimension(400, 700));
		dialog.pack();
		dialog.setVisible(true);
	}

	private void addCopyMenuToTextarea(JTextArea textArea)
	{
        JPopupMenu menu = new JPopupMenu();
        Action cut = new DefaultEditorKit.CutAction();
        cut.putValue(Action.NAME, "Cut");
        cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
        menu.add( cut );

        Action copy = new DefaultEditorKit.CopyAction();
        copy.putValue(Action.NAME, "Copy");
        copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
        menu.add( copy );

        Action paste = new DefaultEditorKit.PasteAction();
        paste.putValue(Action.NAME, "Paste");
        paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
        menu.add( paste );

        Action selectAll = new SelectAll();
        menu.add( selectAll );
        
        textArea.setComponentPopupMenu( menu );
    }

	private void generateCSVAction()
	{
		int selectedRow = tree.getSelectionRows()[0];
		TreePath path = tree.getPathForRow(selectedRow);
		TreeNode node = (TreeNode)path.getLastPathComponent();

		CsvDialog dialog = new CsvDialog();
		dialog.setVisible(true);
		
		List<String> selected = dialog.getSelectedFields();
		if (selected != null)
		{
			File outputFile = selectCsvFile();
			
			if (outputFile != null)
			{
				CsvDialog.writeCsv(node, selected, outputFile);
			}
		}
	}

	private File selectCsvFile()
	{
		JFileChooser chooser = new JFileChooser();
		FileFilter csvFilter = new FileFilter() {

			@Override
			public boolean accept(File f)
			{
				if (f.isDirectory()) return true;
				
				if (f.getName().endsWith(".csv")) return true;
				
				return false;
			}

			@Override
			public String getDescription()
			{
				return "CSV (csv) Files";
			}
			
		};
		
		chooser.addChoosableFileFilter(csvFilter);
		chooser.setFileFilter(csvFilter);
		
		int returnVal = chooser.showSaveDialog(this);
		
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = chooser.getSelectedFile();
            return file;
        }
        
        return null;
	}

	// from: https://stackoverflow.com/questions/30682416/java-right-click-copy-cut-paste-on-textfield
    @SuppressWarnings("serial")
	static class SelectAll extends TextAction
    {
        public SelectAll()
        {
            super("Select All");
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
        }

        public void actionPerformed(ActionEvent e)
        {
            JTextComponent component = getFocusedComponent();
            component.selectAll();
            component.requestFocusInWindow();
        }
    }
}
