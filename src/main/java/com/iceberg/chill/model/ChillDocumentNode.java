package com.iceberg.chill.model;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

/**
 * One of the problems with JTree and TreeModel is that to perform certain
 * operations, you often need to call functions on one or the other.
 * However, especially from a TreeNode, the node doesn't know about the
 * tree or the model.
 * 
 * So we're using this to define an interface to help a tree node get
 * the tree and model. The default implements will be to call one's
 * parent, and keep passing the call up parents to the top-level parent
 * who will know the tree and model and just manage it there. At least
 * then we can isolate the awkwardness to a single place.
 */
public interface ChillDocumentNode
{
	/**
	 * We don't really care about this method. This interface (ChillDocumentNode)
	 * is only placed on items that implement TreeNode, which already has this
	 * method. We've only put it here so we can provide default implementations
	 * for getTree and getModel (by delegating to their parent). It's a bit of
	 * a hack, but it's a convenient one.
	 * 
	 * @return
	 */
	TreeNode getParent();
	
	/**
	 * Called to get the tree of which this node is a part.
	 * @return
	 */
	default JTree getTree()
	{
		if (getParent() instanceof ChillDocumentNode)
		{
			ChillDocumentNode parent = (ChillDocumentNode)getParent();
			return parent.getTree();
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Called to get the model of which this node is a part.
	 * @return
	 */
	default TreeModel getModel()
	{
		if (getParent() instanceof ChillDocumentNode)
		{
			ChillDocumentNode parent = (ChillDocumentNode)getParent();
			return parent.getModel();
		}
		else
		{
			return null;
		}
	}

}
