package com.iceberg.chill.model.xsd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.TreeMap;

import javax.swing.tree.TreeNode;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.*;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeNode;
import com.iceberg.chill.model.TypedTreeNode;
import com.iceberg.chill.model.xml.XmlNodeHelper;

public class XsdSimpleTypeNode implements TreeNode, ChillTreeNode, ChillDocumentNode, TypedTreeNode
{
	TreeNode parent;
	XmlSchemaSimpleType content;
	List<TreeNode> children = new ArrayList<TreeNode>();
	
	public XsdSimpleTypeNode(TreeNode parent, XmlSchemaSimpleType eType)
	{
		this.parent = parent;
		this.content = eType;
		
		identifyChildren();
	}
	
	public XsdSimpleTypeNode(TreeNode parent, XsdSimpleTypeNode copy)
	{
		this.parent = parent;
		this.content = copy.content;
		
		identifyChildren();
	}

	@Override
	public String toString()
	{
		return content.getName();
	}
	
	@Override
	public String getTextContent()
	{
		return XmlNodeHelper.generateTextContent(content.getElementReference());
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		if (content.getQName() != null)
		{
			map.put("name(local)", content.getQName().getLocalPart());
			map.put("namespace", content.getQName().getNamespaceURI());
		}
		
		addPatternToPropertiesIfWeHaveOne(map);
		
		return map;
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(content.getAnnotation());
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return children.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return children.size() == 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(children);
	}

	// -------------------------------------------------------
	// other methods
	// -------------------------------------------------------

	private void identifyChildren()
	{
		XsdTreeNodeFactory.addSimpleTypeChildren(this, children, content);
	}

	private void addPatternToPropertiesIfWeHaveOne(TreeMap<String, String> map)
	{
		XmlSchemaSimpleTypeContent internalContent = content.getContent();
		if (internalContent instanceof XmlSchemaSimpleTypeRestriction)
		{
			XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction)internalContent;
			for (XmlSchemaFacet facet : restriction.getFacets())
			{
				if (facet instanceof XmlSchemaPatternFacet)
				{
					XmlSchemaPatternFacet patternFacet = (XmlSchemaPatternFacet)facet;
					map.put("(pattern)", (String)patternFacet.getValue());
				}
			}
		}
	}

	@Override
	public QName getContentQName()
	{
		return content.getQName();
	}

	@Override
	public XmlSchemaSimpleType getSimpleContent()
	{
		return this.content;
	}
}
