package com.iceberg.chill.model.xsd;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeMenuProvider;
import com.iceberg.chill.model.ChillTreeNode;
import com.iceberg.chill.model.TypedTreeNode;
import com.iceberg.chill.model.xml.XmlNodeHelper;

public class XsdElementNode implements TreeNode, ChillTreeNode, ChillDocumentNode, ChillTreeMenuProvider, TypedTreeNode
{
	XmlSchemaElement content;
	TreeNode parent;
	List<TreeNode> children = new ArrayList<TreeNode>();
	
	public XsdElementNode(TreeNode parent, XmlSchemaElement content)
	{
		this.parent = parent;
		
		this.content = determineContent(content);
		
		identifyChildren();
	}
	
	public XsdElementNode(TreeNode parent, XsdElementNode copy)
	{
		this.parent = parent;
		
		this.content = copy.content;
		
		identifyChildren();
	}

	/**
	 * We have this method in case we're just a ref, in which case we really
	 * want to use the ref content.
	 * 
	 * @param content
	 * @return
	 */
	private XmlSchemaElement determineContent(XmlSchemaElement content)
	{
		if (content.getRef() != null)
		{
			XmlSchemaElement ref = (XmlSchemaElement)content.getRef().getTarget();
			if (ref != null) return ref;
		}
		
		return content;
	}

	@Override
	public String toString()
	{
		return content.getName();
	}
	
	@Override
	public String getTextContent()
	{
		return XmlNodeHelper.generateTextContent(content.getElementReference());
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(content.getAnnotation());
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		if (content.getQName() != null)
		{
			map.put("name(local)", content.getQName().getLocalPart());
			map.put("namespace", content.getQName().getNamespaceURI());
		}

		if (content.getSchemaTypeName() != null) map.put("(type)", content.getSchemaTypeName().getLocalPart());
		
		map.put("minOccurs", Long.toString(content.getMinOccurs()));
		map.put("maxOccurs", (content.getMaxOccurs() != Long.MAX_VALUE
				? Long.toString(content.getMaxOccurs()) : "unbounded"));
		
		if (content.getDefaultValue() != null) map.put("defaultValue", content.getDefaultValue());
		if (content.getFixedValue() != null) map.put("fixedValue", content.getFixedValue());
		
		return map;
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return children.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return children.size() == 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(children);
	}

	// -------------------------------------------------------
	// other methods
	// -------------------------------------------------------

	/**
     * Find a Node with a given QName.
     * We need to find an xs:element where the name=the qname we were given.
     *
     * @param node parent node
     * @param name QName of the child we need to find
     * @return child node
     */
    public static Node findNode(Node node, QName name){
        if(name.getNamespaceURI().equals(node.getNamespaceURI()) &&
           name.getLocalPart().equals(node.getLocalName()))
            return node;
        NodeList children = node.getChildNodes();
        for(int i=0;i<children.getLength();i++){
            Node ret = findNode(children.item(i), name);
            if(ret != null)
                return ret;
        }
        return null;
    }

    /**
     * When creating our node, identifies and creates the child nodes
     * for this element.
     * 
     */
	private void identifyChildren()
	{
		XmlSchemaType contentType = content.getSchemaType();
		if (contentType instanceof XmlSchemaSimpleType)
		{
			XsdTreeNodeFactory.addSimpleTypeChildren(this, children, (XmlSchemaSimpleType)content.getSchemaType());
		}
		else if (contentType instanceof XmlSchemaComplexType)
		{
			XsdTreeNodeFactory.addComplexTypeChildren(this, children, (XmlSchemaComplexType)content.getSchemaType());
		}
		// else I guess we don't have any children
	}

	@Override
	public Map<String, ActionListener> getMenuOptions()
	{
		Map<String, ActionListener> map = new HashMap<String, ActionListener>();
		
		if (content.getSchemaTypeName() != null)
		{
			map.put("Goto Type ", (l)->gotoType());
		}
		
		return map;
	}

	private void gotoType()
	{
		TreeNode typeNode = XsdHelper.getNodeForType(getModel(), content.getSchemaTypeName());
		
		DefaultTreeModel model = (DefaultTreeModel)getModel();
		TreePath typePath = new TreePath(model.getPathToRoot(typeNode));
		
		getTree().makeVisible(typePath);
		getTree().setSelectionPath(typePath);
		getTree().scrollPathToVisible(typePath);
	}

	@Override
	public QName getContentQName()
	{
		return content.getQName();
	}

}
