package com.iceberg.chill.model.xsd;

import java.util.*;

import javax.swing.tree.TreeNode;

import org.apache.ws.commons.schema.XmlSchemaObject;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillIconSelector;
import com.iceberg.chill.model.ChillTreeNode;

public class XsdGroupNode implements TreeNode, ChillTreeNode, Collection<TreeNode>, ChillIconSelector, ChillDocumentNode
{
	XmlSchemaObject content;
	TreeNode parent;
	String name;
	List<TreeNode> children = new ArrayList<TreeNode>();
	String iconName = null;

	public XsdGroupNode(TreeNode parent, String name, XmlSchemaObject schemaObj)
	{
		this.content = schemaObj;
		this.parent = parent;
		this.name = name;
	}

	public XsdGroupNode(TreeNode parent, String name, XmlSchemaObject schemaObj, String iconName)
	{
		this.content = schemaObj;
		this.parent = parent;
		this.name = name;
		this.iconName = iconName;
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return children.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return children.size() == 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(children);
	}

	@Override
	public String getTextContent()
	{
		return "";
		//return generateXsdText(content.getParent(), content.getQName());
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		return new TreeMap<String, String>();
	}

	@Override
	public String getAnnotation()
	{
		return null;
	}

	@Override
	public int size()
	{
		return children.size();
	}

	@Override
	public boolean isEmpty()
	{
		return children.size() == 0;
	}

	@Override
	public boolean contains(Object o)
	{
		return children.contains(o);
	}

	@Override
	public Iterator<TreeNode> iterator()
	{
		return children.iterator();
	}

	@Override
	public Object[] toArray()
	{
		return children.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		return children.toArray(a);
	}

	@Override
	public boolean add(TreeNode e)
	{
		return children.add(e);
	}

	@Override
	public boolean remove(Object o)
	{
		return children.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		return children.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends TreeNode> c)
	{
		return children.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		return children.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		return children.retainAll(c);
	}

	@Override
	public void clear()
	{
		children.clear();
	}

	@Override
	public String toString()
	{
		return name;
	}

	@Override
	public String getIconName()
	{
		return iconName;
	}
}
