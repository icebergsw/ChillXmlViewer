package com.iceberg.chill.model.xsd;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.tree.TreeNode;

import org.apache.ws.commons.schema.*;

/**
 * Creates the node types for a schema tree.
 */
public class XsdTreeNodeFactory
{

	/**
	 * The root item for a schema is actually an Xml document
	 * (from a whole schema). So it's a different type than
	 * the rest of the elements and types.
	 * 
	 * @param schema
	 * @return
	 */
	public static TreeNode buildRootForSchema(XmlSchema schema)
	{
		XsdDocumentNode root = new XsdDocumentNode(schema);		
		return root;
	}

	public static TreeNode buildRootForSchema(XmlSchema schema, Predicate<Object> filter)
	{
		XsdDocumentNode root = new XsdDocumentNode(schema, filter);		
		return root;
	}

	/**
	 * Creates an element node.
	 * 
	 * @param elem
	 * @return
	 */
	public static XsdElementNode createNodeForElement(TreeNode parent, XmlSchemaElement elem)
	{
		return new XsdElementNode(parent, elem);
	}

	/**
	 * Creates a type node.
	 * 
	 * @param eType
	 * @return
	 */
	public static TreeNode createNodeForType(TreeNode parent, XmlSchemaType eType)
	{
		if (eType instanceof XmlSchemaSimpleType)
		{
			return new XsdSimpleTypeNode(parent, (XmlSchemaSimpleType)eType);
		}
		else if (eType instanceof XmlSchemaComplexType)
		{
			return new XsdComplexTypeNode(parent, (XmlSchemaComplexType)eType);
		}
		else
		{
			Logger.getLogger("chill").log(Level.WARNING, "Unknown schema object type requested for type: " + eType.getClass().getSimpleName());
			return null;
		}
	}

	/**
	 * A node that contains a group of other elements, with a name.
	 * 
	 * maybe we'll add some other data later.
	 * 
	 * @param parent
	 * @param name
	 * @param schemaObj
	 * @return
	 */
	private static XsdGroupNode createNodeForGroup(TreeNode parent, String name, XmlSchemaObject schemaObj, String iconName)
	{
		return new XsdGroupNode(parent, name, schemaObj, iconName);
	}

	public static TreeNode createEnumerationNode(TreeNode parent, XmlSchemaEnumerationFacet facet)
	{
		return new XsdEnumerationNode(parent, facet);
	}

	private static TreeNode createAttribute(TreeNode parent, XmlSchemaAttribute schemaObj)
	{
		return new XsdAttributeNode(parent, schemaObj);
	}

	/**
	 * If this is a simple type, identify the children for a simple type.
	 * 
	 * Adds any children to the child list.
	 */
	public static void addSimpleTypeChildren(TreeNode parent, List<TreeNode> children, XmlSchemaSimpleType simpleType)
	{
		checkForEnumerationItems(parent, children, simpleType);
	}

	private static void checkForEnumerationItems(TreeNode parent, List<TreeNode> children, XmlSchemaSimpleType content)
	{
		XmlSchemaSimpleTypeContent internalContent = content.getContent();
		if (internalContent instanceof XmlSchemaSimpleTypeRestriction)
		{
			XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction)internalContent;
			for (XmlSchemaFacet facet : restriction.getFacets())
			{
				if (facet instanceof XmlSchemaEnumerationFacet)
				{
					children.add(XsdTreeNodeFactory.createEnumerationNode(parent, (XmlSchemaEnumerationFacet)facet));
				}
			}
		}
	}

	/**
	 * If this is a complex type, get the children for a complex type.
	 * 
	 * Adds any children to the child list.
	 * 
     * Child nodes are identified by particles in the XmlSchema structure.
	 */
	public static void addComplexTypeChildren(TreeNode parent,
			List<TreeNode> children, XmlSchemaComplexType complexType)
	{
		// first add attributes
		for (XmlSchemaAttributeOrGroupRef attr : complexType.getAttributes())
		{
			addVariableGroupTypeChildren(parent, children, attr);
		}

		// then add particles, which is sub-types and sub-elements
		addParticleChildren(parent, children, complexType.getParticle());

		// add complex content
		addComplexContent(parent, children, complexType);
	}
	
	public static void addBaseTypeChildren(TreeNode parent, List<TreeNode> children, XmlSchemaType baseType)
	{
		if (baseType instanceof XmlSchemaSimpleType)
		{
			addSimpleTypeChildren(parent, children,	(XmlSchemaSimpleType) baseType);
		}
		else if (baseType instanceof XmlSchemaComplexType)
		{
			addComplexTypeChildren(parent, children, (XmlSchemaComplexType) baseType);
		}
		else
		{
			// TODO - warning about unknown type
		}
	}

	private static void addComplexContent(TreeNode parent,
			List<TreeNode> children, XmlSchemaComplexType complexType)
	{
		if (complexType.getContentModel() != null)
		{
			XmlSchemaContent content = complexType.getContentModel().getContent();
			if (content instanceof XmlSchemaComplexContentExtension)
			{
                XmlSchemaComplexContentExtension ext = (XmlSchemaComplexContentExtension)content;
                addBaseTypeChildren(parent, children, complexType.getParent().getTypeByName(ext.getBaseTypeName()));
                addParticleChildren(parent, children, ext.getParticle());
			}
			else if (content instanceof XmlSchemaComplexContentRestriction)
			{
				// TODO - implement
				// note the problem with restriction is it limits the children from the base
				// so in theory we'd have to use this to exclude other children
			}
			else if (content instanceof XmlSchemaSimpleContentExtension)
			{
				addSimpleContentExtension(parent, children, (XmlSchemaSimpleContentExtension) content);
			}
			else if (content instanceof XmlSchemaSimpleContentRestriction)
			{
				// TODO - implement
				// note the problem with restriction is it limits the children from the base
				// so in theory we'd have to use this to exclude other children
			}
			else
			{
				Logger.getLogger("chill").log(Level.WARNING, "Unexpected complex content type: " + content.getClass().getSimpleName());
			}
		}
	}
	
	private static void addParticleChildren(TreeNode parent, List<TreeNode> children, XmlSchemaParticle particle)
	{
        addVariableGroupTypeChildren(parent, children, particle);
	}

	private static void addVariableGroupTypeChildren(TreeNode parent, Collection<TreeNode> children, XmlSchemaObject schemaObj)
	{
		if (schemaObj == null)
		{
			// just do nothing, apparently this can happen sometimes
		}
		else if (schemaObj instanceof XmlSchemaAny)
		{
			addAnyGroup(parent, children, (XmlSchemaAny)schemaObj);
		}
		else if (schemaObj instanceof XmlSchemaElement)
		{
			// just add the element
			children.add(createNodeForElement(parent, (XmlSchemaElement)schemaObj));
		}
		else if (schemaObj instanceof XmlSchemaGroupRef)
		{
			addVariableGroupTypeChildren(parent, children, ((XmlSchemaGroupRef)schemaObj).getParticle());
		}
		else if (schemaObj instanceof XmlSchemaGroup)
		{
			addVariableGroupTypeChildren(parent, children, ((XmlSchemaGroup)schemaObj).getParticle());
		}
		else if (schemaObj instanceof XmlSchemaSequence)
		{
			addSequenceChildren(parent, children, (XmlSchemaSequence)schemaObj);
		}
		else if (schemaObj instanceof XmlSchemaAll)
		{
			addAllItemsChildren(parent, children, (XmlSchemaAll)schemaObj);
		}
		else if (schemaObj instanceof XmlSchemaChoice)
		{
			addChoiceChildren(parent, children, (XmlSchemaChoice)schemaObj);
		}
		else if (schemaObj instanceof XmlSchemaAttribute)
		{
			children.add(createAttribute(parent, (XmlSchemaAttribute)schemaObj));
		}
		else
		{
			Logger.getLogger("chill").log(Level.INFO, "Unknown type in group for class type: " + schemaObj.getClass().getSimpleName());
		}
	}

	private static void addSimpleContentExtension(TreeNode parent,
			List<TreeNode> children, XmlSchemaSimpleContentExtension content)
	{
		for (XmlSchemaAttributeOrGroupRef attr : content.getAttributes())
		{
			addVariableGroupTypeChildren(parent, children, attr);
		}
	}

	/**
	 * For a sequence, we just add each of the children in order;
	 * we don't add anything to represent the "sequence" element.
	 * 
	 * @param parent
	 * @param children
	 * @param sequence
	 */
	private static void addSequenceChildren(TreeNode parent, Collection<TreeNode> children, XmlSchemaSequence sequence)
	{
		for (XmlSchemaSequenceMember sequenceObj : sequence.getItems())
		{
			addVariableGroupTypeChildren(parent, children, (XmlSchemaObject)sequenceObj);
		}
	}

	/**
	 * Adds a choice object.
	 * In this case, we add a node named "choice" with the set of choice
	 * children underneath that.
	 * 
	 * @param parent
	 * @param children
	 * @param schemaObj
	 */
	private static void addChoiceChildren(TreeNode parent, Collection<TreeNode> children, XmlSchemaChoice choice)
	{
		// TODO - create and use a group node icon
		XsdGroupNode choiceNode = createNodeForGroup(parent, "choice", choice, null);
		children.add(choiceNode);
		for (XmlSchemaChoiceMember member : choice.getItems())
		{
			if (member instanceof XmlSchemaObject)
			{
				addVariableGroupTypeChildren(parent, choiceNode, (XmlSchemaObject)member);
			}
		}
	}

	/**
	 * For an "all" item, add the all item, then add everything under it to it.
	 * 
	 * @param parent
	 * @param children
	 * @param all
	 */
	private static void addAllItemsChildren(TreeNode parent, Collection<TreeNode> children, XmlSchemaAll all)
	{
		// TODO - create and use an "all" node icon
		XsdGroupNode allNode = createNodeForGroup(parent, "all", all, null);
		children.add(allNode);
		for (XmlSchemaAllMember member : all.getItems())
		{
			if (member instanceof XmlSchemaObject)
			{
				addVariableGroupTypeChildren(parent, allNode, (XmlSchemaObject)member);
			}
		}
	}

	private static void addAnyGroup(TreeNode parent, Collection<TreeNode> children, XmlSchemaAny any)
	{
		XsdGroupNode anyNode = createNodeForGroup(parent, "any", any, "XSDAny.gif");
		children.add(anyNode);
	}

	public static TreeNode buildRootForType(TreeNode node, XmlSchema schema)
	{
		XsdTypeHierarchyDocNode rootNode = new XsdTypeHierarchyDocNode(node, schema);
		return rootNode;
	}

}
