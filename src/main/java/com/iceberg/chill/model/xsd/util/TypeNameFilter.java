package com.iceberg.chill.model.xsd.util;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;

public class TypeNameFilter implements Predicate<Object>
{
	Pattern pattern;

	public TypeNameFilter(String typeNameRegex)
	{
		pattern = Pattern.compile(typeNameRegex);
	}
	
	@Override
	public boolean test(Object obj)
	{
		if (isMatchingElement(obj))
		{
			return true;
		}
		if (isMatchingType(obj))
		{
			return true;
		}
		return false;

	}

	private boolean isMatchingElement(Object obj)
	{
		if (obj instanceof XmlSchemaElement)
		{
			XmlSchemaElement content = (XmlSchemaElement)obj;
			if (content.getSchemaTypeName() != null)
			{
				Matcher m = pattern.matcher(content.getSchemaTypeName().getLocalPart());
				return m.matches();
			}
		}
		
		return false;
	}

	private boolean isMatchingType(Object obj)
	{
		if (obj instanceof XmlSchemaSimpleType)
		{
			return isMatchingSimpleType((XmlSchemaSimpleType)obj);
		}
		if (obj instanceof XmlSchemaComplexType)
		{
			return isMatchingComplexType((XmlSchemaComplexType)obj);
		}
		return false;
	}

	private boolean isMatchingSimpleType(XmlSchemaSimpleType content)
	{
		Matcher m = pattern.matcher(content.getQName().getLocalPart());
		return m.matches();
	}

	private boolean isMatchingComplexType(XmlSchemaComplexType content)
	{
		Matcher m = pattern.matcher(content.getQName().getLocalPart());
		return m.matches();
	}

}
