package com.iceberg.chill.model.xsd;

import java.awt.Container;
import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.xml.transform.stream.StreamSource;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaCollection;

import com.iceberg.chill.ChillTreePane;
import com.iceberg.chill.model.ChillTreeModel;

public class XsdTreeModel extends DefaultTreeModel implements ChillTreeModel
{
	private static final long serialVersionUID = 1L;

	JTree tree;
	XmlSchema schema;
	
	public XsdTreeModel(TreeNode root, XmlSchema schema)
	{
		super(root);
		this.schema = schema;
	}

	public XmlSchema getSchema()
	{
		return schema;
	}
	
	public static TreeModel loadFile(File file)
	{
		XmlSchema schema = tryLoadSchemaFromFile(file);
		
		XsdTreeModel model = createModelFromSchema(schema);

		// presumably could do this in constructor, but have it here for now
		//model.addTreeModelListener(model);
		
		return model;

	}

	private static XmlSchema tryLoadSchemaFromFile(File file)
	{
		try
		{
			return loadSchemaFromFile(file);
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	private static XmlSchema loadSchemaFromFile(File file) throws FileNotFoundException
	{
		InputStream is = new FileInputStream(file);
		XmlSchemaCollection schemaCol = new XmlSchemaCollection();
		schemaCol.setBaseUri(file.getParent());
		schemaCol.setPreserveElementReferences(true);
		XmlSchema schema = schemaCol.read(new StreamSource(is));
		return schema;
	}

	public static XsdTreeModel createModelFromSchema(XmlSchema schema)
	{
		try
		{
			TreeNode rootNode = XsdTreeNodeFactory.buildRootForSchema(schema);
			XsdTreeModel model = new XsdTreeModel(rootNode, schema);
			return model;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	public static XsdTreeModel createTypeHierarchyModelForNode(TreeNode node, XmlSchema schema)
	{
		try
		{
			TreeNode rootNode = XsdTreeNodeFactory.buildRootForType(node, schema);
			XsdTreeModel model = new XsdTreeModel(rootNode, schema);
			return model;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}
	
	public static XsdTreeModel createFilteredModelFromSchema(XmlSchema schema, Predicate<Object> filter)
	{
		try
		{
			TreeNode rootNode = XsdTreeNodeFactory.buildRootForSchema(schema, filter);
			XsdTreeModel model = new XsdTreeModel(rootNode, schema);
			return model;
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}
	
	// ----------------------------------------------------
	// other methods
	// ----------------------------------------------------

	/**
	 * When this is called, we tell the root node about both the tree
	 * and the model (in support of the ChillDocumentNode interface).
	 */
	@Override
	public void setTree(JTree tree)
	{
		this.tree = tree;
		
		if (this.getRoot() instanceof XsdDocumentNode)
		{
			XsdDocumentNode rootNode = (XsdDocumentNode)this.getRoot();
			rootNode.setTreeAndModel(tree, this);
		}
		
		// expand the three top-level entries for element, complex, and simple types
		tree.expandRow(3);
		tree.expandRow(2);
		tree.expandRow(1);
	}

	/**
	 * When we change the structure for a node, we want to preserve
	 * the state of the tree otherwise.
	 */
	@Override
	public void nodeStructureChanged(TreeNode node)
	{
		List<TreePath> expandedPaths = getExpandedPathsExcludingThisNode(node);
		
		super.nodeStructureChanged(node);
		
		restoreExpandedPaths(expandedPaths);
	}

	private List<TreePath> getExpandedPathsExcludingThisNode(TreeNode node)
	{
		TreePath root = getTreePathForRootNode();
		
		List<TreePath> pathsToKeep = new ArrayList<TreePath>();
		
		Enumeration<TreePath> expandedPath = tree.getExpandedDescendants(root);
		while (expandedPath.hasMoreElements())
		{
			TreePath path = expandedPath.nextElement();
			if (!doesPathContainNode(path, node))
			{
				pathsToKeep.add(path);
			}
		}
		
		return pathsToKeep;
	}

	private void restoreExpandedPaths(List<TreePath> expandedPaths)
	{
		for (TreePath path : expandedPaths)
		{
			tree.expandPath(path);
		}
	}

	private TreePath getTreePathForRootNode()
	{
		TreeNode[] nodes = { (TreeNode)this.getRoot() };
		TreePath path = new TreePath(nodes);
		
		return path;
	}

	/**
	 * turns out we don't need to do this
	 * it works just fine with "extra" paths in there that don't exist
	 * so we're leaving this as a placeholder, but don't need this method
	 * 
	 * @param path
	 * @param node
	 * @return
	 */
	private boolean doesPathContainNode(TreePath path, TreeNode node)
	{
		return false;
	}
	
	public Container getTypeHierarchyTreeForNode(TreeNode node)
	{
		ChillTreePane treePane = new ChillTreePane();
		TreeModel typeTreeModel = XsdTreeModel.createTypeHierarchyModelForNode(node, schema);
		treePane.setModel(typeTreeModel);
		return treePane;
	}
	
	public Container createFilteredDialogObject(XmlSchema schema, Predicate<Object> filter)
	{
		ChillTreePane treePane = new ChillTreePane();
		TreeModel typeTreeModel = XsdTreeModel.createFilteredModelFromSchema(schema, filter);
		treePane.setModel(typeTreeModel);
		return treePane;
	}
}
