package com.iceberg.chill.model.xsd.util;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ws.commons.schema.XmlSchemaEnumerationFacet;
import org.apache.ws.commons.schema.XmlSchemaFacet;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeContent;
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeRestriction;

public class EnumValueFilter implements Predicate<Object>
{
	private String filterRegex;
	Pattern pattern;
	
	public EnumValueFilter(String regex)
	{
		this.filterRegex = regex;
		pattern = Pattern.compile(filterRegex);
	}
	
	@Override
	public boolean test(Object obj)
	{
		if (obj instanceof XmlSchemaSimpleType)
		{
			XmlSchemaSimpleType content = (XmlSchemaSimpleType)obj;
			
			XmlSchemaSimpleTypeContent internalContent = content.getContent();
			if (internalContent instanceof XmlSchemaSimpleTypeRestriction)
			{
				XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction)internalContent;
				for (XmlSchemaFacet facet : restriction.getFacets())
				{
					if (facet instanceof XmlSchemaEnumerationFacet)
					{
						String enumName = (String)facet.getValue();
						Matcher m = pattern.matcher(enumName);
						return m.matches();
					}
				}
			}
		}
		
		return false;
	}

}
