package com.iceberg.chill.model.xsd;

import java.util.Enumeration;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.tree.TreeNode;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaAttribute;
import org.apache.ws.commons.schema.XmlSchemaSerializer.XmlSchemaSerializerException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeNode;
import com.iceberg.chill.model.xml.XmlNodeHelper;

/**
 * An attribute node is, presumably, always a leaf.
 */
public class XsdAttributeNode implements TreeNode, ChillTreeNode, ChillDocumentNode
{
	TreeNode parent;
	XmlSchemaAttribute content;
	
	public XsdAttributeNode(TreeNode parent, XmlSchemaAttribute schemaObj)
	{
		this.parent = parent;
		this.content = schemaObj;
	}

	@Override
	public String toString()
	{
		return content.getName();
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return null;
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return -1;
	}

	@Override
	public boolean getAllowsChildren()
	{
		return false;
	}

	@Override
	public boolean isLeaf()
	{
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return null;
	}

	@Override
	public String getTextContent()
	{
		return generateXsdText(content.getParent(), content.getQName());
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		if (content.getQName() != null)
		{
			map.put("name(local)", content.getQName().getLocalPart());
			map.put("namespace", content.getQName().getNamespaceURI());
		}
		
		if (content.getSchemaTypeName() != null) map.put("(type)", content.getSchemaTypeName().getLocalPart());
		
		if (content.getUse() != null) map.put("use", content.getUse().toString());
		
		return map;
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(content.getAnnotation());
	}

	// -------------------------------------------------------
	// other methods
	// -------------------------------------------------------
	
	/**
	 * Called to generate the text of this node. To do so, we need to find the
	 * DOM object and then display that. Here's the tricky part:
	 * The QName we get from our type is the type within the schema. What we
	 * need to find is an xsd:attribute node where the name is our type (and
	 * presumably have to deal with URI prefixes).
	 * 
	 * @param parent
	 * @param qname
	 * @return
	 */
	public static String generateXsdText(XmlSchema parent, QName qname)
	{
		try
		{
			return unsafeGenerateXsdText(parent, qname);
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.WARNING, ex.getMessage(), ex);
			return "";
		}
	}

    private static String unsafeGenerateXsdText(XmlSchema parent, QName qname) throws XmlSchemaSerializerException
	{
		Document doc = parent.getSchemaDocument();
		
		// TODO - since an attribute name could easily be repeated across many elements,
		// this is not likely to be sufficient for finding an attribute, as it will just
		// find an instance of this attribute name. We'd need to contain the parent to
		// be our particular element
		// should work for element, complex/simple types, but not attributes
		Node node = XsdHelper.findTypeForQName(doc, qname, "attribute");
		
		return XmlNodeHelper.generateTextContent(node);
	}

}
