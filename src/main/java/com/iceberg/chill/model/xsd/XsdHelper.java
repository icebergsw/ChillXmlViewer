package com.iceberg.chill.model.xsd;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaSerializer.XmlSchemaSerializerException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.iceberg.chill.model.TypedTreeNode;
import com.iceberg.chill.model.xml.XmlNodeHelper;

/**
 * A class to store some common XSD helper functions.
 */
public class XsdHelper
{
	final static String schemaURI = "http://www.w3.org/2001/XMLSchema";
	
	public static Node findTypeForQName(Document doc, QName qname, String xsdName)
	{
		Map<String, String> namespaces = new HashMap<String, String>();
		
		Node foundNode = findElementForQNameUsingNamespaces(doc.getDocumentElement(), qname, namespaces);
		
		return foundNode;
	}

	/**
	 * Recursive function to find items in a node, searching children as needed.
	 * We keep track of namespaces so that we can adjust those as needed to be
	 * smart about current namespace prefixes.
	 * 
	 * Note that if a node overrides a prefix, we'll have to duplicate the map
	 * and pass down the overridden one.
	 * 
	 * @param node
	 * @param qname
	 * @param namespaces
	 * @param xsdName 
	 * @return
	 */
	static Node findElementForQNameUsingNamespaces(
			Node node, QName qname,	Map<String, String> namespaces)
	{
		Map<String, String> localNamespaces = getNamespacesForCurrentNode(node, namespaces);
		
//		if ((isNodeOfExpectedTypeBasedOnName(node, xsdName)) && (doesNodeNameMatchQName(node, qname, localNamespaces)))
		if (doesNodeNameMatchQName(node, qname, localNamespaces))
		{
			// we've found it!
			return node;
		}
		else
		{
				// recursive search children
				NodeList children = node.getChildNodes();
				for (int i = 0; i < children.getLength(); i++)
				{
					try
					{
						Node found = findElementForQNameUsingNamespaces(children.item(i), qname, localNamespaces);
						if (found != null) return found;
					}
					catch (Exception ex)
					{
						// not found return null
					}
				}
		}
		
		// not found
		return null;
	}

	/**
	 * Adds any namespace attributes we find to the current map.
	 * If that results in nothing changing, we just return the
	 * namespace we were given.
	 * 
	 * If anything changes (new namespace definitions in this node)
	 * then we return a map with the new namespace definitions.
	 * That is, we never alter the current map.
	 * 
	 * Namespaces are defined by xmlns-prefixed attribute names.
	 * 
	 * @param node
	 * @param namespaces
	 * @return
	 */
	static Map<String, String> getNamespacesForCurrentNode(Node node, Map<String, String> namespaces)
	{
		// shortcut for the most common case
		if (!node.hasAttributes()) return namespaces;
		
		NamedNodeMap attributes = node.getAttributes();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			if (attributes.item(i).getNodeName().startsWith("xmlns"))
			{
				return getUpdatedNamespaces(node, namespaces);
			}
			else if (attributes.item(i).getNodeName().equals("targetNamespace"))
			{
				return getUpdatedNamespaces(node, namespaces);
			}
		}
		
		return namespaces;
	}

	/**
	 * Updates the namespace map with new namespaces from the current node's attributes.
	 * 
	 * @param node
	 * @param namespaces
	 * @return
	 */
	static Map<String, String> getUpdatedNamespaces(Node node, Map<String, String> namespaces)
	{
		// start by copying all the existing ones
		Map<String, String> newNamespaces = new HashMap<String, String>();
		newNamespaces.putAll(namespaces);

		NamedNodeMap attributes = node.getAttributes();
		for (int i = 0; i < attributes.getLength(); i++)
		{
			Node attrNode = attributes.item(i);
			
			// check for the default namespace
			if (attrNode.getNodeName().equals("targetNamespace"))
			{
				newNamespaces.put("",  attrNode.getNodeValue());
			}
			else if (attrNode.getNodeName().startsWith("xmlns:"))
			{
				String nsName = attrNode.getNodeName().replace("xmlns:", "");
				newNamespaces.put(nsName,  attrNode.getNodeValue());
			}
		}
		
		return newNamespaces;
	}

	static boolean isNodeOfExpectedTypeBasedOnName(Node node, String xsdName)
	{
		return ((schemaURI.equals(node.getNamespaceURI())) && (node.getLocalName().equals(xsdName)));
	}

	static boolean doesNodeNameMatchQName(Node node, QName qname, Map<String, String> namespaces)
	{
		// get the name attribute for the node
		String nodeName = getAttributeNameForNode(node);
		if (nodeName != null)
		{
			String nodeNamespace = getNamespaceFromNameValue(nodeName, namespaces);
			if (qname.getNamespaceURI().equals(nodeNamespace))
			{
				if (qname.getLocalPart().equals(getLocalNameFromNameValue(nodeName)))
				{
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Literally gets the value of the attribute named "name"
	 * @param node
	 * @return
	 */
	static String getAttributeNameForNode(Node node)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null)
		{
			for (int i = 0; i < attributes.getLength(); i++)
			{
				Node attrNode = attributes.item(i);
				
				if (attrNode.getNodeName().equals("name"))
				{
					return attrNode.getNodeValue();
				}
			}
		}
		return null;
	}

	/**
	 * We either get the default namespace or theone for the matching prefix.
	 * To put in context what we're doing here, if we had:
	 *  <xs:element name="parameters">
	 * Then nodeName is "parameters" and we get the default "" namespace.
	 * If we had:
	 *  <xs:element name="ns1:parameters">
	 * then nodeName is "ns1:parameters" and we get the ns1 URI.
	 * 
	 * @param nodeName
	 * @param namespaces
	 * @return
	 */
	static String getNamespaceFromNameValue(String nodeName, Map<String, String> namespaces)
	{
		if (nodeName.contains(":"))
		{
			String ns = nodeName.split(":")[0];
			return namespaces.get(ns);
		}
		else
		{
			return namespaces.get("");
		}
	}

	/**
	 * Given a name of the form either:
	 *  abc
	 *  ns1:abc
	 * We return the abc part of the name.
	 * 
	 * @param nodeName
	 * @return
	 */
	static String getLocalNameFromNameValue(String nodeName)
	{
		if (nodeName.contains(":"))
		{
			return nodeName.substring(nodeName.indexOf(":") + 1);
		}
		else
		{
			return nodeName;
		}
	}
	
	/**
	 * Called to generate the text of this node. To do so, we need to find the
	 * DOM object and then display that. Here's the tricky part:
	 * The QName we get from our type is the type within the schema. What we
	 * need to find is an xsd:element node where the name is our type (and
	 * presumably have to deal with URI prefixes).
	 * 
	 * @param parent
	 * @param qname
	 * @return
	 */
	public static String generateXsdText(XmlSchema parent, TreePath path)
	{
		try
		{
			return unsafeGenerateXsdText(parent, path);
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.WARNING, ex.getMessage(), ex);
			return "";
		}
	}

    private static String unsafeGenerateXsdText(XmlSchema parent, TreePath path) throws XmlSchemaSerializerException
	{
		Document doc = parent.getSchemaDocument();
		
		for (int i = 0; i < path.getPathCount(); i++)
		{
			Map<String, String> namespaces = new HashMap<String, String>();
			
			Node node = heirarchicalFindNode(doc, path, i, namespaces);
			
			if (node != null)
			{
				return XmlNodeHelper.generateTextContent(node);
			}
		}
		
		return "";	// not found
	}

    private static Node heirarchicalFindNode(Node rootNode, TreePath path, int pathDepth, Map<String, String> namespaces)
	{
    	if ((pathDepth+1) < path.getPathCount())
    	{
        	QName nextNodeQname = getQNameForNode(path.getPathComponent(pathDepth + 1));
        	
    		Node found = findElementForQNameUsingNamespaces(rootNode, nextNodeQname, namespaces);
    		
    		if (found != null) heirarchicalFindNode(found, path, pathDepth + 1, namespaces);
    	}
    	else
    	{
    		return rootNode;
    	}
    	
    	return null;
    }

	private static QName getQNameForNode(Object treeNode)
	{
		if (treeNode instanceof TypedTreeNode)
		{
			TypedTreeNode typedNode = (TypedTreeNode)treeNode;
			return typedNode.getContentQName();
		}
		
		return null;
	}

	public static TreeNode getNodeForType(TreeModel model, QName schemaTypeName)
	{
		TreeNode root = (TreeNode)model.getRoot();
		TreeNode matchingNode = recursiveSearchNodes(root, schemaTypeName);
		
		if (matchingNode != null)
		{
			return matchingNode;
		}
		else
		{
			// TODO - warning, error, popup, what?
			Logger.getLogger("chill").log(Level.WARNING, "No type found for QName: " + schemaTypeName.toString());
		}
		
		return null;
	}

	private static TreeNode recursiveSearchNodes(TreeNode node, QName schemaTypeName)
	{
		if (isMatchingNode(node, schemaTypeName)) return node;
		
		for (int i = 0; i < node.getChildCount(); i++)
		{
			TreeNode match = recursiveSearchNodes(node.getChildAt(i), schemaTypeName);
			if (match != null) return match;
		}
		
		return null;
	}

	private static boolean isMatchingNode(TreeNode node, QName schemaTypeName)
	{
		if (node instanceof TypedTreeNode)
		{
			TypedTreeNode typeNode = (TypedTreeNode) node;
			
			if (typeNode.getContentQName().equals(schemaTypeName))
			{
				return true;
			}
		}
		return false;
	}

}
