package com.iceberg.chill.model.xsd;

import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaComplexContentExtension;
import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaContent;
import org.apache.ws.commons.schema.XmlSchemaContentModel;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaSimpleContentExtension;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;
import org.apache.ws.commons.schema.XmlSchemaType;

/**
 * Some functions to help do operations related to the Apache XmlSchema package.
 */
public class SchemaHelper
{

	public static XmlSchemaType getComplexContentBaseTypeFromSchema(XmlSchemaComplexType complexType, XmlSchema schema)
	{
		XmlSchemaContent content = complexType.getContentModel().getContent();
		if (content instanceof XmlSchemaComplexContentExtension)
		{
			XmlSchemaComplexContentExtension ext = (XmlSchemaComplexContentExtension) content;
			QName baseTypeName = ext.getBaseTypeName();
			if (baseTypeName != null)
			{
				return SchemaHelper.findTypeInSchema(baseTypeName, schema);
			}
		}
		
		// if not found
		return null;
	}

	public static XmlSchemaType getSimpleContentBaseTypeFromSchema(XmlSchemaComplexType complexType, XmlSchema schema)
	{
		XmlSchemaContent content = complexType.getContentModel().getContent();
		if (content instanceof XmlSchemaSimpleContentExtension)
		{
			XmlSchemaSimpleContentExtension ext = (XmlSchemaSimpleContentExtension) content;
			QName baseTypeName = ext.getBaseTypeName();
			if (baseTypeName != null)
			{
				return SchemaHelper.findTypeInSchema(baseTypeName, schema);
			}
		}

		// if not found
		return null;
	}

	public static XmlSchemaType findTypeInSchema(QName baseTypeName, XmlSchema schema)
	{
		for (XmlSchemaType eType : schema.getSchemaTypes().values())
		{
			if (baseTypeName.equals(eType.getQName()))
			{
				return (XmlSchemaType)eType;
			}
		}
		return null;
	}

	public static QName getbaseTypeQName(XmlSchemaElement elem)
	{
		XmlSchemaType contentType = elem.getSchemaType();
		return getbaseTypeQName(contentType);
	}

	public static QName getbaseTypeQName(XmlSchemaType schemaType)
	{
		if (schemaType instanceof XmlSchemaComplexType)
		{
			XmlSchemaComplexType complexType = (XmlSchemaComplexType)schemaType;
			return getComplexTypeBaseTypeQName(complexType);
		}
		else if (schemaType instanceof XmlSchemaSimpleType)
		{
			XmlSchemaSimpleType simpleType = (XmlSchemaSimpleType)schemaType;
			return getSimpleTypeBaseTypeQName(simpleType);
		}
		
		return null;
	}

	public static QName getComplexTypeBaseTypeQName(XmlSchemaComplexType complexType)
	{
		XmlSchemaContentModel model = complexType.getContentModel();
		if (model != null)
		{
			XmlSchemaContent content = model.getContent();
			if (content instanceof XmlSchemaSimpleContentExtension)
			{
				XmlSchemaSimpleContentExtension ext = (XmlSchemaSimpleContentExtension) content;
				QName baseTypeName = ext.getBaseTypeName();
				return baseTypeName;
			}
		}
		return null;
	}

	public static QName getSimpleTypeBaseTypeQName(XmlSchemaSimpleType simpleType)
	{
		// I don't think there's anything to do here, but we're going
		// to leave it just in case we realize we need it later
		return null;
	}
}
