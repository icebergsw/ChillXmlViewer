package com.iceberg.chill.model.xsd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.TreeMap;

import javax.swing.tree.TreeNode;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.*;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeNode;
import com.iceberg.chill.model.TypedTreeNode;
import com.iceberg.chill.model.xml.XmlNodeHelper;

/**
 * This is used to represent types; that is, something like complexType or simpleType,
 * where it's not unto itself a core element definition but a type definition.
 */
public class XsdComplexTypeNode implements TreeNode, ChillTreeNode, ChillDocumentNode, TypedTreeNode
{
	TreeNode parent;
	XmlSchemaComplexType content;
	List<TreeNode> children = new ArrayList<TreeNode>();

	public XsdComplexTypeNode(TreeNode parent, XmlSchemaComplexType content)
	{
		this.parent = parent;
		
		this.content = content;
		
		identifyChildren();
	}

	public XsdComplexTypeNode(TreeNode parent, XsdComplexTypeNode copy)
	{
		this.parent = parent;
		
		this.content = copy.content;
		
		identifyChildren();
	}

	@Override
	public String toString()
	{
		return content.getName();
	}
	
	@Override
	public String getTextContent()
	{
		return XmlNodeHelper.generateTextContent(content.getElementReference());
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(content.getAnnotation());
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		if (content.getQName() != null)
		{
			map.put("name(local)", content.getQName().getLocalPart());
			map.put("namespace", content.getQName().getNamespaceURI());
		}
		
		return map;
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return children.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return children.size() == 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(children);
	}

	// -------------------------------------------------------
	// other methods
	// -------------------------------------------------------

    /**
     * When creating our node, identifies and creates the child nodes
     * for this element.
     * 
     */
	private void identifyChildren()
	{
		XsdTreeNodeFactory.addComplexTypeChildren(this, children, content);
	}

	@Override
	public QName getContentQName()
	{
		return content.getQName();
	}
	
	@Override
	public XmlSchemaComplexType getComplexContent()
	{
		return this.content;
	}
}
