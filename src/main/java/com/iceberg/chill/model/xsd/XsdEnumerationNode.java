package com.iceberg.chill.model.xsd;

import java.util.Enumeration;
import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.apache.ws.commons.schema.XmlSchemaEnumerationFacet;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeNode;

public class XsdEnumerationNode implements TreeNode, ChillTreeNode, ChillDocumentNode
{
	TreeNode parent;
	XmlSchemaEnumerationFacet content;
	
	public XsdEnumerationNode(TreeNode parent, XmlSchemaEnumerationFacet facet)
	{
		this.parent = parent;
		this.content = facet;
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return null;
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return -1;
	}

	@Override
	public boolean getAllowsChildren()
	{
		return false;
	}

	@Override
	public boolean isLeaf()
	{
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return null;
	}

	@Override
	public String getTextContent()
	{
		// nothing meaningful to display
		return "";
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		// always blank
		return new TreeMap<String, String>();
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(content.getAnnotation());
	}

	@Override
	public String toString()
	{
		return (String)content.getValue();
	}
}
