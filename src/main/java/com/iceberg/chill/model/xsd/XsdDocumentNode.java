package com.iceberg.chill.model.xsd;

import java.io.StringWriter;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import org.apache.ws.commons.schema.*;
import org.apache.ws.commons.schema.XmlSchemaSerializer.XmlSchemaSerializerException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ChillTreeNode;
import com.iceberg.chill.model.ContainerTreeNode;

/**
 * Root for the tree, this is the document object.
 * This will contain all elements/types as its children
 * (actually, we contain container types for the types of
 * children we hold, and those hold our children).
 */
public class XsdDocumentNode implements TreeNode, ChillTreeNode, ChillDocumentNode
{
	XmlSchema schema;
	List<TreeNode> allChildren = new ArrayList<TreeNode>();
	JTree myTree;
	TreeModel myModel;
	Predicate<Object> filter;
	
	XsdDocumentNode(XmlSchema schema)
	{
		this(schema, (o) -> { return true; });
	}

	public XsdDocumentNode(XmlSchema schema, Predicate<Object> filter)
	{
		this.schema = schema;
		this.filter = filter;
		
		addAllElements();
		addComplexTypes();
		addSimpleTypes();
	}

	private void addAllElements()
	{
		List<TreeNode> elementNodes = new ArrayList<TreeNode>();

		ContainerTreeNode elementContainer = new ContainerTreeNode("elements", this);

		for (XmlSchemaElement elem : schema.getElements().values())
		{
			if (filter.test(elem))
			{
				XsdElementNode node = XsdTreeNodeFactory.createNodeForElement(elementContainer, elem);
				elementNodes.add(node);
			}
		}

		elementContainer.setChildren(elementNodes);
		
		allChildren.add(elementContainer);
	}
	
	private void addComplexTypes()
	{
		addSchemaTypes(XmlSchemaComplexType.class, "complexTypes");
	}

	private void addSimpleTypes()
	{
		addSchemaTypes(XmlSchemaSimpleType.class, "simpleTypes");
	}

	private void addSchemaTypes(Class<?> selectedType, String containerName)
	{
		List<TreeNode> typeNodes = new ArrayList<TreeNode>();

		ContainerTreeNode typeContainer = new ContainerTreeNode(containerName, this);

		for (XmlSchemaType eType : schema.getSchemaTypes().values())
		{
			if (selectedType.isInstance(eType))
			{
				if (filter.test(eType))
				{
					TreeNode node = XsdTreeNodeFactory.createNodeForType(typeContainer, eType);
					typeNodes.add(node);
				}
			}
		}

		typeContainer.setChildren(typeNodes);
		
		allChildren.add(typeContainer);
	}

	// -----------------------------------------------
	// TreeNode functions
	// -----------------------------------------------

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return allChildren.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return allChildren.size();
	}

	@Override
	public TreeNode getParent()
	{
		return null;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return allChildren.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(allChildren);
	}

	// -----------------------------------------------
	// ChillTreeNode functions
	// -----------------------------------------------

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		try
		{
			unsafeGetNodeProperties(map);
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.INFO, ex.getMessage(), ex);
		}
		
		return map;
	}

	/**
	 * For the document, we want the properties display to show the attributes
	 * of the document node, some are useful, some less so, but makes sense
	 * to display those.
	 * 
	 * @param map
	 * @throws XmlSchemaSerializerException
	 */
	private void unsafeGetNodeProperties(TreeMap<String, String> map) throws XmlSchemaSerializerException
	{
		NamedNodeMap attributes = schema.getSchemaDocument().getDocumentElement().getAttributes();
		
		for (int i = 0; i < attributes.getLength(); i++)
		{
			Node attr = attributes.item(i);
			
			map.put(attr.getNodeName(), attr.getNodeValue());
		}
	}

	/**
	 * Displays the content of the entire schema document.
	 */
	@Override
	public String getTextContent()
	{
		StringWriter wr = new StringWriter();
		schema.write(wr);
		return wr.toString();
	}

	@Override
	public String getAnnotation()
	{
		return AnnotationHelper.getAnnotation(schema.getAnnotation());
	}

	// -----------------------------------------------
	// functions from ChillDocumentNode
	// -----------------------------------------------

	@Override
	public JTree getTree()
	{
		return myTree;
	}

	@Override
	public TreeModel getModel()
	{
		return myModel;
	}

	// -----------------------------------------------
	// other functions
	// -----------------------------------------------

	@Override
	public String toString()
	{
		return "schema";
	}

	/**
	 * Called shortly after creation to set the tree and model.
	 * We hold this information for the entire XSD tree (who pass
	 * calls asking for this information up to us).
	 * 
	 * @param tree
	 * @param model
	 */
	public void setTreeAndModel(JTree tree, TreeModel model)
	{
		this.myTree = tree;
		this.myModel = model;
	}
}
