package com.iceberg.chill.model.xsd;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ws.commons.schema.XmlSchemaAnnotation;
import org.apache.ws.commons.schema.XmlSchemaAnnotationItem;
import org.apache.ws.commons.schema.XmlSchemaAppInfo;
import org.apache.ws.commons.schema.XmlSchemaDocumentation;
import org.w3c.dom.NodeList;

/**
 * This class is here just to do some common processing with annotations
 * on the schema, mostly so we can display them in their own
 * annotation panel, and I don't have to repeat this code. One standard
 * way to produce an annotation.
 */
public class AnnotationHelper
{

	public static String getAnnotation(XmlSchemaAnnotation annotation)
	{
		StringBuilder builder = new StringBuilder();
		
		if (annotation != null)
		{
			for (XmlSchemaAnnotationItem item : annotation.getItems())
			{
				if (item instanceof XmlSchemaAppInfo)
				{
					addAppInfoToAnnotationText(builder, (XmlSchemaAppInfo)item);
				}
				else if (item instanceof XmlSchemaDocumentation)
				{
					addDocumentationToAnnotationText(builder, (XmlSchemaDocumentation)item);
				}
			}
		}
		
		return builder.toString();
	}

	private static void addAppInfoToAnnotationText(StringBuilder builder, XmlSchemaAppInfo info)
	{
		if (info.getSource() != null)
		{
			builder.append(info.getSource());
			builder.append("\n");
		}
	}

	private static void addDocumentationToAnnotationText(StringBuilder builder, XmlSchemaDocumentation doc)
	{
		try
		{
			NodeList nodes = doc.getMarkup();
			
			for (int i = 0; i < nodes.getLength(); i++)
			{
				builder.append(doc.getMarkup().item(i).getNodeValue());
				builder.append("\n");
			}
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.INFO, ex.getMessage(), ex);
		}
	}

}
