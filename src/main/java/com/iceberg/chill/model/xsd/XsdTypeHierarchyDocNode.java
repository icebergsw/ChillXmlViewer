package com.iceberg.chill.model.xsd;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.*;

import com.iceberg.chill.model.ChillDocumentNode;
import com.iceberg.chill.model.ContainerTreeNode;
import com.iceberg.chill.model.TypedTreeNode;

/**
 * Root for the tree, this is the document object.
 * This will contain all elements/types as its children
 * (actually, we contain container types for the types of
 * children we hold, and those hold our children).
 */
public class XsdTypeHierarchyDocNode implements TreeNode, ChillDocumentNode
{
	List<TreeNode> allChildren = new ArrayList<TreeNode>();
	JTree myTree;
	TreeModel myModel;
	XmlSchema schema;
	
	XsdTypeHierarchyDocNode(TreeNode thisType, XmlSchema schema)
	{
		this.schema = schema;
		
		addBaseType(thisType);
		addThisType(thisType);
		addChildTypes(thisType);
	}
	
	private void addBaseType(TreeNode thisType)
	{
		ContainerTreeNode baseTypeContainer = new ContainerTreeNode("baseType", this);

		TreeNode baseTypeNode = findBaseTypeNodeForTypeWithinSchema(baseTypeContainer, thisType, schema);
		if (baseTypeNode != null)
		{
			List<TreeNode> baseTypes = new ArrayList<TreeNode>();
			baseTypes.add(baseTypeNode);
			baseTypeContainer.setChildren(baseTypes);
		}

		allChildren.add(baseTypeContainer);
	}

	private void addThisType(TreeNode thisType)
	{
		ContainerTreeNode thisTypeContainer = new ContainerTreeNode("thisType", this);

		List<TreeNode> thisTypeList = new ArrayList<TreeNode>();
		thisTypeList.add(duplicateTypeNode(thisType));
		thisTypeContainer.setChildren(thisTypeList);

		allChildren.add(thisTypeContainer);
	}

	private void addChildTypes(TreeNode thisType)
	{
		ContainerTreeNode childTypesContainer = new ContainerTreeNode("childTypes", this);

		List<TreeNode> childNodes = findChildTypeNodesForTypeWithinSchema(childTypesContainer, thisType, schema);

		childTypesContainer.setChildren(childNodes);
		
		allChildren.add(childTypesContainer);
	}

	// -----------------------------------------------
	// TreeNode functions
	// -----------------------------------------------

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return allChildren.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return allChildren.size();
	}

	@Override
	public TreeNode getParent()
	{
		return null;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return allChildren.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(allChildren);
	}

	// -----------------------------------------------
	// functions from ChillDocumentNode
	// -----------------------------------------------

	@Override
	public JTree getTree()
	{
		return myTree;
	}

	@Override
	public TreeModel getModel()
	{
		return myModel;
	}

	// -----------------------------------------------
	// other functions
	// -----------------------------------------------

	@Override
	public String toString()
	{
		return "schema";
	}

	/**
	 * Called shortly after creation to set the tree and model.
	 * We hold this information for the entire XSD tree (who pass
	 * calls asking for this information up to us).
	 * 
	 * @param tree
	 * @param model
	 */
	public void setTreeAndModel(JTree tree, TreeModel model)
	{
		this.myTree = tree;
		this.myModel = model;
	}
	
	// -----------------------------------------------
	// constructor (builder) functions
	// -----------------------------------------------

	/**
	 * If this node has a base type, create a new node for that base type
	 * and return it.
	 * 
	 * @param thisType
	 * @param thisType2 
	 * @param schema
	 * @return
	 */
	private TreeNode findBaseTypeNodeForTypeWithinSchema(TreeNode parent, TreeNode thisType, XmlSchema schema)
	{
		if (thisType instanceof TypedTreeNode)
		{
			TypedTreeNode typedNode = (TypedTreeNode)thisType;
			
			if (typedNode.getComplexContent() != null)
			{
				XmlSchemaType base = getComplexContentBaseTypeFromSchema(typedNode, schema);
				if (base != null)
				{
					return XsdTreeNodeFactory.createNodeForType(parent, base);
				}
			}
			
			if (typedNode.getSimpleContent() != null)
			{
				XmlSchemaType base = getSimpleContentBaseTypeFromSchema(typedNode, schema);
				if (base != null)
				{
					return XsdTreeNodeFactory.createNodeForType(parent, base);
				}
			}
		}
		
		return new javax.swing.tree.DefaultMutableTreeNode("none");
	}

	private XmlSchemaType getComplexContentBaseTypeFromSchema(TypedTreeNode typedNode, XmlSchema schema)
	{
		try
		{
			XmlSchemaComplexType complexType = typedNode.getComplexContent();
			return SchemaHelper.getComplexContentBaseTypeFromSchema(complexType, schema);
		}
		catch (Exception ex)
		{
			// ignore, probably normal
		}
		return null;
	}

	private XmlSchemaType getSimpleContentBaseTypeFromSchema(TypedTreeNode typedNode, XmlSchema schema)
	{
		try
		{
			XmlSchemaComplexType complexType = typedNode.getComplexContent();
			return SchemaHelper.getSimpleContentBaseTypeFromSchema(complexType, schema);
		}
		catch (Exception ex)
		{
			// ignore, probably normal
		}
		return null;
	}

	/**
	 * Create a new node for the current (thisType) node; essentially
	 * duplicate it, but with this node as a parent.
	 * 
	 * @param thisType
	 * @return
	 */
	private TreeNode duplicateTypeNode(TreeNode thisType)
	{
		try
		{
			// look for a constructor that takes a treenode and its own type,
			// which is a copy constructor with a different parent setting
			Class<?> typeNodeClass = thisType.getClass();
			Constructor<?> typeConstructor = typeNodeClass.getConstructor(TreeNode.class, typeNodeClass);
			TreeNode duplicate = (TreeNode)typeConstructor.newInstance(this, thisType);
			return duplicate;
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.WARNING, "Unable to duplicate type node due to exception: " + ex.getMessage(), ex);
			return null;
		}
	}

	/**
	 * Find all types that are children types (inherit from) this type,
	 * and return a list of nodes for those types (or elements with complex
	 * content, right, or even simple types and content).
	 * 
	 * @param thisType
	 * @param schema
	 * @return
	 */
	private List<TreeNode> findChildTypeNodesForTypeWithinSchema(TreeNode parent, TreeNode thisType, XmlSchema schema)
	{
		List<TreeNode> childTypeNodes = new ArrayList<TreeNode>();
		
		if (thisType instanceof TypedTreeNode)
		{
			TypedTreeNode thisNode = (TypedTreeNode)thisType;
			QName thisQname = thisNode.getContentQName();
			
			findAndAddChildElements(parent, childTypeNodes, thisQname, schema);
			findAndAddChildTypes(parent, childTypeNodes, thisQname, schema);
		}

		return childTypeNodes;
	}

	private void findAndAddChildElements(TreeNode parent,
			List<TreeNode> childTypeNodes, QName thisQname, XmlSchema schema)
	{
		for (XmlSchemaElement elem : schema.getElements().values())
		{
			addElementIfDirectlyOfThisType(parent, childTypeNodes, thisQname, elem);
			addElementIfComplexContentOfThisType(parent, childTypeNodes, thisQname, elem);
		}
	}

	private void addElementIfDirectlyOfThisType(TreeNode parent,
			List<TreeNode> childTypeNodes, QName thisQname,
			XmlSchemaElement elem)
	{
		if (thisQname.equals(elem.getSchemaTypeName()))
		{
			XsdElementNode node = XsdTreeNodeFactory.createNodeForElement(parent, elem);
			childTypeNodes.add(node);
		}
	}

	private void addElementIfComplexContentOfThisType(TreeNode parent,
			List<TreeNode> childTypeNodes, QName thisQname,
			XmlSchemaElement elem)
	{
		QName baseTypeQName = SchemaHelper.getbaseTypeQName(elem);
		if (thisQname.equals(baseTypeQName))
		{
			XsdElementNode node = XsdTreeNodeFactory.createNodeForElement(parent, elem);
			childTypeNodes.add(node);
		}
	}

	private void findAndAddChildTypes(TreeNode parent,
			List<TreeNode> childTypeNodes, QName thisQname, XmlSchema schema)
	{
		for (XmlSchemaType eType : schema.getSchemaTypes().values())
		{
			QName baseTypeQName = SchemaHelper.getbaseTypeQName(eType);
			if (thisQname.equals(baseTypeQName))
			{
				TreeNode node = XsdTreeNodeFactory.createNodeForType(parent, eType);
				childTypeNodes.add(node);
			}
		}
	}

}
