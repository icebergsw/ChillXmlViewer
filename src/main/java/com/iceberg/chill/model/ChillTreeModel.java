package com.iceberg.chill.model;

import javax.swing.JTree;

/**
 * An interface for defining functions we want in our own tree model.
 */
public interface ChillTreeModel
{
	/**
	 * Called to tell the model about our tree.
	 * @param tree
	 */
	void setTree(JTree tree);
}
