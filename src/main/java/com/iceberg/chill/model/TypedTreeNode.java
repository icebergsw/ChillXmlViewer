package com.iceberg.chill.model;

import javax.xml.namespace.QName;

import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;

/**
 * Defines a TreeNode that has a well known QName as a type.
 * Basically this tree node defines a type, and this interface
 * lets us get the type from the tree node.
 */
public interface TypedTreeNode
{
	QName getContentQName();
	
	/**
	 * Returns complex content if we have any, null if we do not.
	 * @return
	 */
	default XmlSchemaComplexType getComplexContent() { return null; }
	
	/**
	 * Returns simple content if we have any, null if we do not.
	 * @return
	 */
	default XmlSchemaSimpleType getSimpleContent() { return null; }
}
