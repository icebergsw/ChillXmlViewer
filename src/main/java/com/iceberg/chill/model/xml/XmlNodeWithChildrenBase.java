package com.iceberg.chill.model.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.iceberg.chill.model.ChillTreeNode;

/**
 * This serves as the base type for node types that contain children.
 * It uses the standard XML child node manipulation to figure out and
 * return the child information.
 */
public abstract class XmlNodeWithChildrenBase implements TreeNode, ChillTreeNode
{
	TreeNode parent;
	Node content;

	ArrayList<Node> children = new ArrayList<Node>();
	TreeNode[] childNodes;
	
	XmlNodeWithChildrenBase(TreeNode parent, Node node)
	{
		this.parent = parent;
		this.content = node;
		
		selectChildren(node);
		childNodes = new TreeNode[children.size()];
	}

	@Override
	public TreeNode getChildAt(int childIndex)
	{
		if (childNodes[childIndex] == null)
		{
			childNodes[childIndex] = XmlTreeNodeFactory.buildTreeNodeForDocumentObject(this, children.get(childIndex));
		}
		
		return childNodes[childIndex];
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		for (int i = 0; i < childNodes.length; i++)
		{
			if (childNodes[i].equals(node))
			{
				return i;
			}
		}
		
		return -1;
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return children.size() == 0;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		ArrayList<TreeNode> enumArray = new ArrayList<TreeNode>();
		
		for (int i = 0; i < childNodes.length; i++)
		{
			enumArray.add(getChildAt(i));
		}
		
		return Collections.enumeration(enumArray);
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		map.put("(type)", content.getClass().getSimpleName());
		
		addNodePropertiesToMap(content, map);
		
		return map;
	}

	@Override
	public String getTextContent()
	{
		return XmlNodeHelper.generateTextContent(content);
	}

	/**
	 * If we have an annotation child, display the content of annotation/documentation.
	 */
	@Override
	public String getAnnotation()
	{
		return XmlNodeHelper.getAnnotationTextForNode(content);
	}

	/**
	 * If someone is using the base, they should override this method to add any custom
	 * properties for the type to the map for this node.
	 * 
	 * It is OK to do nothing.
	 * 
	 * We don't have a default implementation because we want people to remember
	 * to do this.
	 * 
	 * @param content
	 * @param map
	 */
	abstract void addNodePropertiesToMap(Node content, TreeMap<String, String> map);

	/**
	 * We don't just take every child as valid. We exclude some of them. This method
	 * filters to only ones we care about and adds those to our children list, from
	 * which the rest of this class will operate.
	 * 
	 * @param node
	 * @return
	 */
	private void selectChildren(Node node)
	{
		NodeList childNodes = node.getChildNodes();
		
		for (int i = 0; i < childNodes.getLength(); i++)
		{
			Node child = childNodes.item(i);
			
			if (canAccept(child))
			{
				children.add(child);
			}
		}
	}

	/**
	 * This method does the filtering. Here's what we filter:
	 * 
	 * 	- text nodes with only whitespace content
	 * 
	 * @param child
	 * @return
	 */
	private boolean canAccept(Node child)
	{
		if (isTextNodeWithOnlyWhitespace(child)) return false;
		
		return true;
	}

	private boolean isTextNodeWithOnlyWhitespace(Node child)
	{
		if (child instanceof Text)
		{
			String content = child.getNodeValue();
			String trimmed = content.trim();
			trimmed = trimmed.replaceAll("[ \t\r\n]*", "");
			
			if (trimmed.equals("")) return true;
		}
		
		return false;
	}

}
