package com.iceberg.chill.model.xml;

import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Comment;
import org.w3c.dom.Node;

public class TreeNodeComment extends XmlNodeLeafBase
{
	Comment content;

	public TreeNodeComment(TreeNode parent, Comment node)
	{
		super(parent, node);
		this.content = node;
	}

	@Override
	void addNodePropertiesToMap(Node content, TreeMap<String, String> map)
	{
		// nothing extra we need to add for a comment
	}

	@Override
	public String toString()
	{
		return "<comment>";
	}

}
