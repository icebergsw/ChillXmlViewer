package com.iceberg.chill.model.xml;

import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlNodeHelper
{

	/**
	 * Standard function to convert from any XML Node to text content.
	 * 
	 * @param xmlNode
	 * @return
	 * @throws TransformerException
	 */
	public static String generateTextContent(Node xmlNode)
	{
		try
		{
			return unsafeGenerateTextContent(xmlNode);
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
	}

	private static String unsafeGenerateTextContent(Node xmlNode) throws TransformerException
	{
		DOMSource domSource = new DOMSource(xmlNode);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(domSource, result);
		
		return writer.toString();
	}

	public static String getAnnotationTextForNode(Node xmlNode)
	{
		try
		{
			return unsafeGetAnnotationTextForNode(xmlNode);
		}
		catch (Exception ex)
		{
			Logger.getLogger("chill").log(Level.WARNING, ex.getMessage(), ex);
			return "";
		}
	}

	private static String unsafeGetAnnotationTextForNode(Node xmlNode)
	{
		Node annotationNode = findChildNodeByName(xmlNode, "annotation");
		Node documentationNode = findChildNodeByName(annotationNode, "documentation");
		return documentationNode.getNodeValue();
	}

	private static Node findChildNodeByName(Node node, String nodeName)
	{
		NodeList children = node.getChildNodes();
		for (int i = 0; i < children.getLength(); i++)
		{
			if (children.item(i).getNodeName().equals(nodeName))
			{
				return children.item(i);
			}
		}
		
		throw new java.util.NoSuchElementException();
	}
}
