package com.iceberg.chill.model.xml;

import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


public class TreeNodeElement extends XmlNodeWithChildrenBase
{
	Element content;

	public TreeNodeElement(TreeNode parent, Element node)
	{
		super(parent, node);
		this.content = node;
	}

	@Override
	void addNodePropertiesToMap(Node content, TreeMap<String, String> map)
	{
		NamedNodeMap attributes = content.getAttributes();
		
		for (int i = 0; i < attributes.getLength(); i++)
		{
			Node attr = attributes.item(i);
			
			map.put(attr.getNodeName(), attr.getNodeValue());
		}
	}

	@Override
	public String toString()
	{
		return "element: " + content.getNodeName();
	}
}
