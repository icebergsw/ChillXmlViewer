package com.iceberg.chill.model.xml;

import java.util.Enumeration;
import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Node;

import com.iceberg.chill.model.ChillTreeNode;

/**
 * TreeNode base type for any custom type that is a leaf node without children.
 * This is simple because most of the methods have static return values in this
 * case.
 */
public abstract class XmlNodeLeafBase implements TreeNode, ChillTreeNode
{
	TreeNode parent;
	Node content;
	
	XmlNodeLeafBase(TreeNode parent, Node node)
	{
		this.parent = parent;
		this.content = node;
	}
	
	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return null;
	}

	@Override
	public int getChildCount()
	{
		return 0;
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return 0;
	}

	@Override
	public boolean getAllowsChildren()
	{
		return false;
	}

	@Override
	public boolean isLeaf()
	{
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return null;
	}

	@Override
	public TreeMap<String, String> getNodeProperties()
	{
		TreeMap<String, String> map = new TreeMap<String, String>();
		
		map.put("(type)", content.getClass().getSimpleName());
		
		addNodePropertiesToMap(content, map);
		
		return map;
	}

	@Override
	public String getTextContent()
	{
		return XmlNodeHelper.generateTextContent(content);
	}

	@Override
	public String getAnnotation()
	{
		return XmlNodeHelper.getAnnotationTextForNode(content);
	}

	/**
	 * If someone is using the base, they should override this method to add any custom
	 * properties for the type to the map for this node.
	 * 
	 * It is OK to do nothing.
	 * 
	 * We don't have a default implementation because we want people to remember
	 * to do this.
	 * 
	 * @param content
	 * @param map
	 */
	abstract void addNodePropertiesToMap(Node content, TreeMap<String, String> map);

}
