package com.iceberg.chill.model.xml;

import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Node;

public class TreeNodeCData extends XmlNodeLeafBase
{
	CDATASection content;
	
	public TreeNodeCData(TreeNode parent, CDATASection node)
	{
		super(parent, node);
		this.content = node;
	}

	@Override
	void addNodePropertiesToMap(Node content, TreeMap<String, String> map)
	{
		
	}

	@Override
	public String toString()
	{
		return "<CDATA>";
	}
}
