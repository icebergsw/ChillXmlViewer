package com.iceberg.chill.model.xml;

import javax.swing.tree.TreeNode;

import org.w3c.dom.*;

/**
 * An XML document contains lots of different kinds of elements.
 * This factory builds the correct type of node for each element type, that way
 * we can seamlessly have different tree node types per element type, since we
 * want to display some differently than others.
 * 
 * Also, in order to enable certain functions (view of things), we need to have
 * some extra smarts in each tree node type as well, so our tree nodes implement
 * some extra interfaces that we can reference elsewhere.
 * 
 * See @see{https://docs.oracle.com/javase/7/docs/api/org/w3c/dom/Node.html}
 * for useful information about how to handle each node type, and a list of
 * the various node types we have to support.
 */
public class XmlTreeNodeFactory
{
	public static TreeNode buildTreeNodeForDocumentObject(TreeNode parent, Node node)
	{
		if (node.getNodeType() == Node.CDATA_SECTION_NODE)
		{
			return new TreeNodeCData(parent, (CDATASection)node);
		}
		else if (node.getNodeType() == Node.COMMENT_NODE)
		{
			return new TreeNodeComment(parent, (Comment)node);
		}
		else if (node.getNodeType() == Node.TEXT_NODE)
		{
			return new TreeNodeText(parent, (Text)node);
		}
		else if (node.getNodeType() == Node.ELEMENT_NODE)
		{
			return buildTreeNodeForElement(parent, (Element)node);
		}
		else
		{
			// if we don't know what else to do, or don't need special processing
			return new TreeNodeNode(parent, node);
		}
	}

	private static TreeNode buildTreeNodeForElement(TreeNode parent, Element node)
	{
		return buildXmlElement(parent, node);
	}

	private static TreeNode buildXmlElement(TreeNode parent, Element node)
	{
		return new TreeNodeElement(parent, node);
	}
}
