package com.iceberg.chill.model.xml;

import java.io.File;
import java.io.IOException;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XmlTreeModel extends DefaultTreeModel implements TreeModelListener
{
	private static final long serialVersionUID = 1L;

	/*
	 * This is the actual org.w3c XML document loaded from file (as a DOM object).
	 */
	Document document;
	
	public XmlTreeModel(TreeNode root, Document doc)
	{
		super(root);
		this.document = doc;
	}

	public static TreeModel loadFile(File file)
	{
		Document doc = tryLoadSelectedFileIntoDOM(file);

		TreeNode rootNode = XmlTreeNodeFactory.buildTreeNodeForDocumentObject(null, doc.getDocumentElement());
		XmlTreeModel model = new XmlTreeModel(rootNode, doc);

		// presumably could do this in constructor, but have it here for now
		model.addTreeModelListener(model);
		
		return model;
	}

	private static Document tryLoadSelectedFileIntoDOM(File file)
	{
		try
		{
			return loadSelectedFileIntoDOM(file);
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	private static Document loadSelectedFileIntoDOM(File file) throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document document = db.parse(file);
		return document;
	}

	// ----------------------------------------------------
	// functions from TreeModelListener
	// ----------------------------------------------------

	@Override
	public void treeNodesChanged(TreeModelEvent e)
	{
	}

	@Override
	public void treeNodesInserted(TreeModelEvent e)
	{
	}

	@Override
	public void treeNodesRemoved(TreeModelEvent e)
	{
	}

	@Override
	public void treeStructureChanged(TreeModelEvent e)
	{
	}

	// ----------------------------------------------------
	// other methods
	// ----------------------------------------------------

}
