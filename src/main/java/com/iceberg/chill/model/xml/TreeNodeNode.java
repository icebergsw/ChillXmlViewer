package com.iceberg.chill.model.xml;

import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Node;

public class TreeNodeNode extends XmlNodeWithChildrenBase
{
	Node content;

	public TreeNodeNode(TreeNode parent, Node node)
	{
		super(parent, node);
		this.content = node;
	}

	@Override
	void addNodePropertiesToMap(Node content, TreeMap<String, String> map)
	{
		
	}

	@Override
	public String toString()
	{
		return "node: " + content.getNodeName();
	}

}
