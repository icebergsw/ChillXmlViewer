package com.iceberg.chill.model.xml;

import java.util.TreeMap;

import javax.swing.tree.TreeNode;

import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class TreeNodeText extends XmlNodeLeafBase
{
	Text content;

	public TreeNodeText(TreeNode parent, Text node)
	{
		super(parent, node);
		this.content = node;
	}

	@Override
	void addNodePropertiesToMap(Node content, TreeMap<String, String> map)
	{
	}

	@Override
	public String toString()
	{
		return "<text>";
	}

}
