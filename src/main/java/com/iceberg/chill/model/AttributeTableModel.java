package com.iceberg.chill.model;

import java.util.ArrayList;
import java.util.TreeMap;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Takes a hashtable of keys and values and turns those into a properties table.
 * We use a TreeMap because this should mean the values are sorted for us already.
 */
public class AttributeTableModel implements TableModel
{
	TreeMap<String, String> properties;
	String[] keys;
	ArrayList<TableModelListener> listeners = new ArrayList<TableModelListener>();
	
	public AttributeTableModel(TreeMap<String, String> nodeProperties)
	{
		this.properties = nodeProperties;
		
		keys = nodeProperties.keySet().toArray(new String[]{});
	}

	@Override
	public int getRowCount()
	{
		return properties.size();
	}

	@Override
	public int getColumnCount()
	{
		return 2;
	}

	@Override
	public String getColumnName(int columnIndex)
	{
		if (columnIndex == 0) return "Attribute";
		if (columnIndex == 1) return "Value";
		return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if (columnIndex == 0)
		{
			return getPropertyName(rowIndex);
		}
		else
		{
			return getPropertyValue(rowIndex);
		}
	}

	private String getPropertyName(int rowIndex)
	{
		return keys[rowIndex];
	}

	private String getPropertyValue(int rowIndex)
	{
		return properties.get(keys[rowIndex]);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex)
	{
		throw new RuntimeException("Model is read-only.");
	}

	@Override
	public void addTableModelListener(TableModelListener l)
	{
		listeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l)
	{
		listeners.remove(l);
	}

}
