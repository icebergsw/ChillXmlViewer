package com.iceberg.chill.model;

import java.awt.event.ActionListener;
import java.util.Map;

/**
 * For any given ChillTreeNode, indicates that it provides custom
 * menu options. This interface is used to provide the custom
 * menu options for a particular node type.
 */
public interface ChillTreeMenuProvider
{
	/**
	 * Returns a map containing menu item names (text to display)
	 * and a callback to the function that gets called when the
	 * menu item is selected.
	 * 
	 * @return
	 */
	Map<String, ActionListener> getMenuOptions();
}
