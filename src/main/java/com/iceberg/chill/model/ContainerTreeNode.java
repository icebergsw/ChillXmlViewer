package com.iceberg.chill.model;

import java.awt.event.ActionListener;
import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

/**
 * This is a simple container tree node type, has a name and a fixed set of children.
 * Basically it's not a MutableTreeNode (not mutable), which is the default provided
 * by Swing, so we create our own to not be mutable.
 */
public class ContainerTreeNode implements TreeNode, ChillTreeMenuProvider, ChillDocumentNode
{
	List<TreeNode> allChildren;
	List<TreeNode> children;
	String name;
	TreeNode parent;
	
	Comparator<TreeNode> sortMethod = null;
	Predicate<TreeNode> filterMethod = null;
	
	String currentFilterRegex = ".*";
	
	public ContainerTreeNode(String name, TreeNode parent)
	{
		this.name = name;
		this.parent = parent;		
	}
	
	public void setChildren(List<TreeNode> children)
	{
		this.allChildren = children;
		
		sortAndFilterChildren();
	}
	
	@Override
	public TreeNode getChildAt(int childIndex)
	{
		return children.get(childIndex);
	}

	@Override
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public TreeNode getParent()
	{
		return parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		return children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren()
	{
		return true;
	}

	@Override
	public boolean isLeaf()
	{
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration children()
	{
		return Collections.enumeration(children);
	}

	@Override
	public String toString()
	{
		return this.name;
	}
	
	public void setSortMethod(Comparator<TreeNode> sortMethod)
	{
		this.sortMethod = sortMethod;
	}
	
	public void setFilterMethod(Predicate<TreeNode> filterMethod)
	{
		this.filterMethod = filterMethod;
	}
	
	/**
	 * After setting a sort or filter method, this method should be called
	 * to update the content of this container node.
	 * 
	 * Also, after doing that, the tree's treeNodesChanged method should
	 * be called with this node as its input, in order to tell the tree
	 * that we've changed.
	 */
	public void refresh()
	{
		this.sortAndFilterChildren();
	}
	
	/**
	 * Called to (potentially) sort the children in any array we're given
	 * before we use them.
	 */
	private void sortAndFilterChildren()
	{		
		// if we don't have a filter, just create one that accepts everything
		if (filterMethod == null) filterMethod = new Predicate<TreeNode>() {
			@Override
			public boolean test(TreeNode t)
			{
				return true;
			}
		};
		
		if (sortMethod != null)
		{
			// sort
			children = allChildren.stream().filter(filterMethod).sorted(sortMethod).collect(Collectors.toList());
		}
		else
		{
			// don't sort
			children = allChildren.stream().filter(filterMethod).collect(Collectors.toList());
		}
	}

	@Override
	public Map<String, ActionListener> getMenuOptions()
	{
		Map<String, ActionListener> map = new LinkedHashMap<String, ActionListener>();
		
		map.put("Filter", (l)->showFilterDialog());
		map.put("Sort A-Z", (l)->sortAscending());
		map.put("Sort Z-A", (l)->sortDescending());
		
		return map;
	}

	private void showFilterDialog()
	{
		String filterString = (String)JOptionPane.showInputDialog(
				"Enter filter regex",
				currentFilterRegex);
		
		if (filterString != null)
		{
			currentFilterRegex = filterString;
			final Pattern p = Pattern.compile(currentFilterRegex);
			setFilterMethod(new Predicate<TreeNode>() {
				@Override
				public boolean test(TreeNode t)
				{
					return testFilterPattern(p, t);
				}
			});
			
		}
		
		refreshContent();
	}

	protected boolean testFilterPattern(Pattern p, TreeNode t)
	{
		Matcher m = p.matcher(t.toString());
		return m.matches();
	}

	private void sortAscending()
	{
		sortMethod = (o1,o2)->(o1.toString().compareTo(o2.toString()));
		refreshContent();
	}

	private void sortDescending()
	{
		sortMethod = (o1,o2)->(o2.toString().compareTo(o1.toString()));
		refreshContent();
	}

	private void refreshContent()
	{
		sortAndFilterChildren();
		DefaultTreeModel dm = (DefaultTreeModel)getModel();
		
		// the XsdTreeModel class preserves the expansion state of the rest of the tree
		dm.nodeStructureChanged(this);
	}
}
