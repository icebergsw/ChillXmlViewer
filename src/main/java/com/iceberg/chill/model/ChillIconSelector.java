package com.iceberg.chill.model;

/**
 * Decorate a TreeNode class with this, and the cell rendered will look
 * for a known icon name. We do this so that we can reuse TreeNode classes
 * without having to create a new one for each type just to get a different
 * icon (which we do when class-based).
 */
public interface ChillIconSelector
{
	/**
	 * Returns the filename of our requested icon.
	 * @return
	 */
	String getIconName();

}
