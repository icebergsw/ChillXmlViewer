package com.iceberg.chill.model;

import java.util.TreeMap;

/**
 * Provides extra operations we care about for a tree node.
 * 
 * 
 */
public interface ChillTreeNode
{
	
	/**
	 * Returns the text associated with this node; this node in text form.
	 * @return
	 */
	String getTextContent();
	
	/**
	 * Returns a map of properties about this node that we think are worth displaying to the user.
	 * @return
	 */
	TreeMap<String, String> getNodeProperties();

	/**
	 * Gets annotation/documentation associated with the node, and fills in the
	 * annotation area with it if there is any.
	 * 
	 * @return
	 */
	String getAnnotation();
}
