package com.iceberg.chill;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.swing.*;
import javax.swing.tree.TreeNode;

import com.iceberg.chill.model.xsd.XsdElementNode;

@SuppressWarnings("serial")
public class CsvDialog extends JDialog
{
	List<String> selected;
	
	JList<CheckBoxItem> checkBoxList;
	CheckBoxItem[] items;
	
	public CsvDialog()
	{
		setModal(true);
		setTitle("Select CSV Fields");
		
		buildItems();
		buildDialog();
	}
	
    private void buildItems()
	{
    	items = new CheckBoxItem[] {
    			new CheckBoxItem("Name"),
    			new CheckBoxItem("Path"),
    			new CheckBoxItem("Schema Type"),
    			new CheckBoxItem("MinOccurs"),
    			new CheckBoxItem("MaxOccurs"),
    			new CheckBoxItem("Optional"),
    			new CheckBoxItem("Description")
    	};
	}

	private void buildDialog()
	{
	    getContentPane().setLayout(new BorderLayout());

	    addContentPanel();
	    addActionButtons();
	    
		setResizable(true);
		setPreferredSize(new Dimension(300, 400));
		pack();
	}

	private void addContentPanel()
	{
	    JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.add(new JLabel("Select the fields to include in the CSV"));
		
		checkBoxList = new JList<CheckBoxItem>(items);
		checkBoxList.setCellRenderer(new CheckBoxCellRenderer());
		checkBoxList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		checkBoxList.addMouseListener(new MouseAdapter() {
	        @Override
	        public void mouseClicked(MouseEvent event) {
	        	handleMouseClicked(event);
	        }
	      });
		panel.add(checkBoxList);
		
		JScrollPane scroll = new JScrollPane(panel);
		getContentPane().add(scroll, BorderLayout.CENTER);
	}

	private void handleMouseClicked(MouseEvent event)
	{
        int index = checkBoxList.locationToIndex(event.getPoint());
        CheckBoxItem item = (CheckBoxItem)checkBoxList.getModel().getElementAt(index);
        item.isChecked = !item.isChecked; // toggle state
        checkBoxList.repaint(checkBoxList.getCellBounds(index, index)); // Repaint cell
	}

	private void addActionButtons()
	{
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				captureSelectedCheckboxes();
				CsvDialog.this.setVisible(false);
				CsvDialog.this.dispose();
			}
        });
        buttonPanel.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				selected = null;
				CsvDialog.this.setVisible(false);
				CsvDialog.this.dispose();
			}
        });
        buttonPanel.add(cancelButton);
	}

	protected void captureSelectedCheckboxes()
	{
		selected = new ArrayList<String>();
		
		for (int i = 0; i < items.length; i++)
		{
			if (items[i].isChecked)
			{
				selected.add(items[i].name);
			}
		}
	}

	public List<String> getSelectedFields()
	{
		return selected;
	}
	
	static class CheckBoxCellRenderer extends JCheckBox implements ListCellRenderer<CheckBoxItem>
    {
		@Override
		public Component getListCellRendererComponent(
				JList<? extends CheckBoxItem> list, CheckBoxItem value, int index,
				boolean isSelected, boolean cellHasFocus)
		{
            setComponentOrientation(list.getComponentOrientation());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setSelected(value.isChecked);
            setEnabled(list.isEnabled());
            setText(value.name);  

            return this;
		}
    }

	static class CheckBoxItem
	{
		public CheckBoxItem() {}
		public CheckBoxItem(String name)
		{
			this.name = name;
			this.isChecked = false;
		}
		String name;
		boolean isChecked;
	}

	/**
	 * Takes a list of selected check names and an output file and writes all elements
	 * out to CSV based on that input.
	 * 
	 * @param node 
	 * @param selected2
	 * @param outputFile
	 */
	public static void writeCsv(TreeNode node, List<String> selected, File outputFile)
	{
		StringBuilder sb = new StringBuilder();
		
		writeCsvColumnHeaders(sb, selected);
		recursiveWriteCsvNode(node, sb, selected, "");
		
		writeCsvFile(sb, outputFile);
	}

	private static void writeCsvColumnHeaders(StringBuilder sb,	List<String> selected)
	{
		for (int i = 0; i < selected.size(); i++)
		{
			if (i > 0) sb.append(",");
			sb.append(selected.get(i));
		}
	}

	private static void recursiveWriteCsvNode(TreeNode node, StringBuilder sb, List<String> selected, String path)
	{
		// if we have a path, append to it, else start it
		path = (path.length() == 0 ? node.toString() : path + "." + node.toString());

		if (node instanceof XsdElementNode)
		{
			sb.append("\n");
			sb.append(writeElementNodeFields((XsdElementNode)node, selected, path));
		}

		addCsvChildren(sb, node, selected, path);
	}

	/**
	 * Basically we only care about element nodes. The reason is because we care about
	 * fields that actually have to be populated. Types are just there to describe
	 * elements.
	 * 
	 * Creates a single CSV-formatted line of the fields for this element.
	 * 
	 * @param node
	 * @param sb
	 * @param selected 
	 */
	private static String writeElementNodeFields(XsdElementNode node, List<String> selected, String path)
	{
		StringBuilder sb = new StringBuilder();
		
		TreeMap<String, String> properties = node.getNodeProperties();
		
		if (selected.contains("Name")) writeCsvField(sb, node.toString());
		if (selected.contains("Path")) writeCsvField(sb, path);
		if (selected.contains("Schema Type")) writeCsvField(sb, properties.get("(type)"));
		if (selected.contains("MinOccurs")) writeCsvField(sb, properties.get("minOccurs"));
		if (selected.contains("MaxOccurs")) writeCsvField(sb, properties.get("maxOccurs"));
		if (selected.contains("Optional")) writeCsvField(sb, properties.get("minOccurs").equals("0") ? "true" : "false");
		if (selected.contains("Description")) writeCsvField(sb, node.getAnnotation());
		
		return sb.toString();
	}

	private static void writeCsvField(StringBuilder sb, String text)
	{
		// if not the first field, add a separator (comma, as in comma-separated)
		if (sb.length() > 0) sb.append(",");
		
		if (text != null)
		{
			// see if we need delimiters
			if (!text.contains(","))
			{
				// no comma in text, so no delimiter, just write it
				sb.append(text);
			}
			else
			{
				// contains a comma, so we have to wrap in quotes
				sb.append("\"");
				
				if (!text.contains("\""))
				{
					// contains comma, but no quotes, so just surround with quotes (write the text)
					sb.append(text);
				}
				else
				{
					// contains comma and quotes, so escape the quotes in the string
					sb.append(text.replaceAll("\"", "\\\""));
				}
				
				sb.append("\"");
			}
		}
	}

	private static void addCsvChildren(StringBuilder sb, TreeNode node, List<String> selected, String parentPath)
	{
		for (int i = 0; i < node.getChildCount(); i++)
		{
			TreeNode child = node.getChildAt(i);
			
			recursiveWriteCsvNode(child, sb, selected, parentPath);
		}
	}

	private static void writeCsvFile(StringBuilder sb, File outputFile)
	{
		try
		{
			FileWriter fw = new FileWriter(outputFile);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(sb.toString());
			
			bw.close();
			fw.close();
		}
		catch (Exception ex)
		{
			java.util.logging.Logger.getLogger("chill").log(Level.SEVERE, ex.getMessage(), ex);
		}
	}
}
