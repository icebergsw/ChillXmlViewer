package com.iceberg.chill;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.iceberg.chill.model.AttributeTableModel;
import com.iceberg.chill.model.ChillTreeNode;

/**
 * Contains two panels stacked vertically:
 * one for the text content of the currently selected node,
 * and one to display a table of properties of the currently selected node.
 */
public class InformationPanel extends JSplitPane
{
	private static final long serialVersionUID = 1L;
	
	JTextArea textContainer;
	JTable propertiesTable;
	JTextArea annotationContainer;
	final TableModel emptyModel = new DefaultTableModel();

	public InformationPanel(JTree tree)
	{
		super();
		
		initialize();
		
	    tree.addTreeSelectionListener((event)->handleTreeSelectionChanged(event));
	}

	private void initialize()
	{
		createTextContentArea();
		createPropertiesArea();
		createAnnotationsArea();
		createSplitPanes();
	}

	private void createSplitPanes()
	{
    	JScrollPane textScrolling = new JScrollPane(textContainer);
   		JScrollPane propertiesScrolling = new JScrollPane(propertiesTable);
   		JScrollPane annotationScrolling = new JScrollPane(annotationContainer);
   		
   		JSplitPane midBottom = new JSplitPane(JSplitPane.VERTICAL_SPLIT, propertiesScrolling, annotationScrolling);
   		midBottom.setResizeWeight(0.5);
   		
   		this.setLeftComponent(textScrolling);
   		this.setRightComponent(midBottom);
   		this.setOrientation(JSplitPane.VERTICAL_SPLIT);
   		this.setResizeWeight(0.5);
	}

	private void createTextContentArea()
	{
    	textContainer = new JTextArea();
    	textContainer.setEditable(false);
	}

	private void createPropertiesArea()
	{
		propertiesTable = new JTable(emptyModel);
	}

    private void createAnnotationsArea()
	{
    	annotationContainer = new JTextArea();
    	annotationContainer.setEditable(false);
    	annotationContainer.setLineWrap(true);
    	annotationContainer.setWrapStyleWord(true);
	}

	private void handleTreeSelectionChanged(TreeSelectionEvent event)
	{
    	clearInformationPanels();
    	
    	try
    	{
	    	//Object o = tree.getLastSelectedPathComponent();
	    	Object o = event.getNewLeadSelectionPath().getLastPathComponent();
	    	
	    	if (o == null) return;
	    	
	    	if (o instanceof ChillTreeNode)
	    	{
	    		populateInformationPanels((ChillTreeNode)o);
	    	}
    	}
    	catch (Exception ex)
    	{
    		// for now, do nothing
    	}
	}

	private void clearInformationPanels()
	{
		textContainer.setText("");
		propertiesTable.setModel(emptyModel);
		annotationContainer.setText("");
	}

	private void populateInformationPanels(ChillTreeNode treeNode)
	{
		populateTextContent(treeNode);
		populateProperties(treeNode);
		populateAnnotation(treeNode);
	}

	private void populateTextContent(ChillTreeNode treeNode)
	{
		try
		{
			String content = treeNode.getTextContent();
			textContainer.setText(content);
			textContainer.setCaretPosition(0);
		}
		catch (Exception ex)
		{
			textContainer.setText("Error: " + ex.getMessage());
		}
	}

	private void populateProperties(ChillTreeNode treeNode)
	{
		java.util.TreeMap<String, String> nodeProperties = treeNode.getNodeProperties();
		
		if (nodeProperties != null)
		{
			TableModel nodeModel = new AttributeTableModel(nodeProperties);
		
			propertiesTable.setModel(nodeModel);
		}
		else
		{
			propertiesTable.setModel(emptyModel);
		}
	}

	private void populateAnnotation(ChillTreeNode treeNode)
	{
		try
		{
			String content = treeNode.getAnnotation();
			annotationContainer.setText(content);
			annotationContainer.setCaretPosition(0);
		}
		catch (Exception ex)
		{
			annotationContainer.setText("Error: " + ex.getMessage());
		}
	}
}
