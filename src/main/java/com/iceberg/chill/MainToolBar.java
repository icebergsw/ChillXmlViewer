package com.iceberg.chill;

import javax.swing.JToolBar;

public class MainToolBar extends JToolBar
{
	private static final long serialVersionUID = 1L;
		
	public MainToolBar()
	{
		super();
		
		initialize();
	}

	private void initialize()
	{
		this.setRollover(true);
		this.setFloatable(false);
		
	}


}
