package com.iceberg.chill;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.iceberg.chill.model.ChillIconSelector;
import com.iceberg.chill.model.xsd.XsdAttributeNode;
import com.iceberg.chill.model.xsd.XsdComplexTypeNode;
import com.iceberg.chill.model.xsd.XsdElementNode;
import com.iceberg.chill.model.xsd.XsdEnumerationNode;
import com.iceberg.chill.model.xsd.XsdSimpleTypeNode;

/**
 * Looks for known types, and based on those known types, adds appropriate icons to
 * the tree as a decoration. If a cell isn't a known type where we have a defined icon,
 * we just use the platform default icons.
 */
public class ChillTreeCellRenderer extends DefaultTreeCellRenderer
{
	private static final long serialVersionUID = 1L;
	
	Map<Class<?>, Icon> iconMap = new HashMap<Class<?>, Icon>();
	Map<String, Icon> iconsByName = new HashMap<String, Icon>();
	
	public ChillTreeCellRenderer()
	{
		super();
		
		loadIcons();
	}

	@Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean isLeaf, int row, boolean focused)
	{
		JLabel c = (JLabel) super.getTreeCellRendererComponent(tree, value, selected, expanded, isLeaf, row, focused);

		// First check if the type specifies an icon, and if so use that
		setIconForType(c, value);

		// but if we have a more specific interface-defined name and icon, use that instead
		setIconForInterface(c, value);
		
		return c;
    }

	private void setIconForType(JLabel c, Object value)
	{
		if (iconMap.containsKey(value.getClass()))
		{
			c.setIcon(iconMap.get(value.getClass()));
		}
	}

	private void setIconForInterface(JLabel c, Object value)
	{
		if (value instanceof ChillIconSelector)
		{
			ChillIconSelector selector = (ChillIconSelector)value;
			String iconName = selector.getIconName();
			if (iconsByName.containsKey(iconName))
			{
				c.setIcon(iconsByName.get(iconName));
			}
		}
	}

	private void loadIcons()
	{
		iconMap.put(XsdAttributeNode.class, createImageIcon("XSDAttribute.gif"));
		iconMap.put(XsdComplexTypeNode.class, createImageIcon("XSDComplexType.gif"));
		iconMap.put(XsdSimpleTypeNode.class, createImageIcon("XSDSimpleType.gif"));
		iconMap.put(XsdElementNode.class, createImageIcon("XSDElement.gif"));
		iconMap.put(XsdEnumerationNode.class, createImageIcon("XSDSimpleEnum.gif"));
		// XSDAny.gif
	}

	private Icon createImageIcon(String iconName)
	{
		java.net.URL imgURL = getClass().getResource("/com/iceberg/chill/icons/" + iconName);
		Icon icon = new ImageIcon(imgURL);
		iconsByName.put(iconName, icon);
		return new ImageIcon(imgURL);
	}

}
