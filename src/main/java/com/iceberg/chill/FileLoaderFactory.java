package com.iceberg.chill;

import java.io.File;

import javax.swing.tree.TreeModel;

import com.iceberg.chill.model.xml.XmlTreeModel;
import com.iceberg.chill.model.xsd.XsdTreeModel;

/**
 * Given a selected input file, chooses the correct loader
 * to load the file into the system and create the correct
 * type of tree model from it.
 */
public class FileLoaderFactory
{
	public static TreeModel loadFile(File file)
	{
		String cwd = System.getProperty("user.dir");

		try
		{
			return loadFileFromItsOwnDirectory(file);
		}
		finally
		{
			System.setProperty("user.dir", cwd);
		}
	}

	private static TreeModel loadFileFromItsOwnDirectory(File file)
	{
		String fileName = file.getName();
		
		System.setProperty("user.dir", file.getParent());
		
		if ((fileName.endsWith(".xml")) || (fileName.endsWith(".XML")))
		{
			return loadXmlModel(file);
		}
		else if ((fileName.endsWith(".xsd")) || (fileName.endsWith(".XSD")))
		{
			return loadXsdModel(file);
		}
		else
		{
			throw new RuntimeException("unknown file type");
		}
	}

	private static TreeModel loadXmlModel(File file)
	{
		TreeModel model = XmlTreeModel.loadFile(file);
		return model;
	}

	private static TreeModel loadXsdModel(File file)
	{
		TreeModel model = XsdTreeModel.loadFile(file);
		return model;
	}
}
