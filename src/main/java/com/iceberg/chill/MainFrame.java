package com.iceberg.chill;

import java.awt.BorderLayout;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import java.awt.event.WindowEvent;
import java.io.File;
import java.util.prefs.Preferences;

public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1L;

	ChillTreePane treePane;
	InformationPanel informationPanel;
	JMenuBar menuBar;
	private JMenu menuFile;
	JToolBar toolBar;
	
	public MainFrame()
	{
		createTree();
		createInformationPanel();
		
		createMainContent();

	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setTitle("Chill XSD Viewer");       
	    this.pack();
	    
	    createMenu();
	    createToolbar();
	    
	    this.setVisible(true);
	}

	private void createMainContent()
	{
    	JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treePane, informationPanel);
	    this.getContentPane().add(splitPane, BorderLayout.CENTER);
	}

	private void createTree()
	{
	    treePane = new ChillTreePane();
	}

	private void createInformationPanel()
	{
		informationPanel = new InformationPanel(treePane.getTree());
	}

	private void createMenu()
	{
	    menuBar = new JMenuBar();
	    setJMenuBar(menuBar);
	    
	    menuFile = new JMenu("File");
	    menuBar.add(menuFile);
	    
	    JMenuItem mntmOpen = new JMenuItem("Open...");
	    mntmOpen.addActionListener((event)->openAction());
	    menuFile.add(mntmOpen);
	    
	    JMenuItem mntmExit = new JMenuItem("Exit");
	    mntmExit.addActionListener((event)->exitAction());
	    menuFile.add(mntmExit);
	}

	private void openAction()
	{
    	Preferences prefs = Preferences.userNodeForPackage( getClass() );
    	String savedPath = prefs.get("xsdPath", ".");

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File(savedPath));
		FileFilter xsdFilter = new FileFilter() {

			@Override
			public boolean accept(File f)
			{
				if (f.isDirectory()) return true;
				
				if (f.getName().endsWith(".xsd")) return true;
				
				return false;
			}

			@Override
			public String getDescription()
			{
				return "Schema (XSD) Files";
			}
			
		};
		
		chooser.addChoosableFileFilter(xsdFilter);
		chooser.setFileFilter(xsdFilter);
		
		int returnVal = chooser.showOpenDialog(this);
		
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = chooser.getSelectedFile();
            
            treePane.openFile(file);
			this.setTitle("Chill XSD Viewer : " + file.getName());
			
        	// save user preference
        	String path = file.getParent();
        	prefs.put("xsdPath", path);
       }
	}

	private void exitAction()
	{
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

	private void createToolbar()
	{
		toolBar = new MainToolBar();
		this.add(toolBar, BorderLayout.NORTH);
	}

	public static void display()
    {
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception ex)
		{
			// if it fails, we take the default
		}
		SwingUtilities.invokeLater(() -> new MainFrame());
    }       

}
