package com.iceberg.chill.model.xsd;

import static org.junit.Assert.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class XsdHelperTest
{
	Map<String, String> namespaces;
	Document document;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception
	{
	}

	@Before
	public void setUp() throws Exception
	{
		namespaces = new HashMap<String, String>();
		namespaces.put("", "/empty");
		namespaces.put("ns1", "/n/s/1");
		namespaces.put("ns2", "/n/s/2");
		namespaces.put("xs", "http://www.w3.org/2001/XMLSchema");
		
		String testDoc = "<root xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:ns1='/n/s/1' xmlns:ns2='/n/s/2'>"
				+ "<xs:element name='ns1:abc'/>"
				+ "<xs:element name='ns2:def'/>"
				+ "</root>";
		InputStream docStream = new ByteArrayInputStream(testDoc.getBytes(StandardCharsets.UTF_8.name()));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		document = db.parse(docStream);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void test_getLocalNameFromNameValue_simple()
	{
		assertEquals("abc", XsdHelper.getLocalNameFromNameValue("abc"));
	}

	@Test
	public void test_getLocalNameFromNameValue_withNS()
	{
		assertEquals("abc", XsdHelper.getLocalNameFromNameValue("ns1:abc"));
	}

	@Test
	public void test_getNamespaceFromNameValue_simple_default()
	{
		assertEquals("/empty", XsdHelper.getNamespaceFromNameValue("abc", namespaces));
	}

	@Test
	public void test_getNamespaceFromNameValue_withNS()
	{
		assertEquals("/n/s/1", XsdHelper.getNamespaceFromNameValue("ns1:abc", namespaces));
	}
	
	@Test
	public void test_getAttributeNameForNode() throws Exception
	{
		Node node = document.getDocumentElement().getFirstChild();
		assertEquals("ns1:abc", XsdHelper.getAttributeNameForNode(node));
	}
	
	@Test
	public void test_doesNodeNameMatchQName_match1()
	{
		Node node = document.getDocumentElement().getFirstChild();
		QName qname = new QName("/n/s/1", "abc");
		assertTrue(XsdHelper.doesNodeNameMatchQName(node, qname, namespaces));
	}
	
	@Test
	public void test_doesNodeNameMatchQName_noMatch_ns()
	{
		Node node = document.getDocumentElement().getFirstChild();
		QName qname = new QName("/n/s/2", "abc");
		assertFalse(XsdHelper.doesNodeNameMatchQName(node, qname, namespaces));
	}
	
	@Test
	public void test_doesNodeNameMatchQName_noMatch_name()
	{
		Node node = document.getDocumentElement().getFirstChild();
		QName qname = new QName("/n/s/1", "def");
		assertFalse(XsdHelper.doesNodeNameMatchQName(node, qname, namespaces));
	}
}
