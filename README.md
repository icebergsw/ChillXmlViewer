Chill XSD Viewer is a project to create a nice XML Schema (XSD) viewer.
This product focuses on presenting schema (xsd) files in a readable,
easy-to-understand manner.

Other XML editors either focus on XML with schema as an add-on, or they
also supply editing functionality, which results in every possible value
being shown and editable, but limits the readability of the data.

This project thus focuses purely on reading and understanding schema (xsd)
files. File content is presented in a tree structure with inline type
expansion. In addition the schema text, annotations, and attributes
(along with some extra properties) are always present.

This project is configured as an Eclipse project, and also includes
a build.gradle file that will build the project. The result is a jar
file. The code embeds and utilizes a custom version of
the Apache XML Schema core library version 2.2 as
a runtime dependency. But do keep in mind it's an embedded update,
not the base Apache library.